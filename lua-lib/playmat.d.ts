/** @noSelfInFile */

export interface Camera {
    sw: number;
    sh: number;
    x: number;
    y: number;
    r: number;
    z: number;
    f: number;
    o: number;
    x1: number;
    y1: number;
    x2: number;
    y2: number;
    buffer: table;

    setRotation: (r: number) => Camera;
    setPosition: (x: number, y: number) => Camera;
    setZoom: (z: number) => Camera;
    setFov: (f: number) => Camera;
    setOffset: (o: number) => Camera;

    getRotation: () => number;
    /** @tupleReturn */
    getPosition: () => [number, number];
    getX: () => number;
    getY: () => number;
    getZoom: () => number;
    getFov: () => number;
    getOffset: () => number;
}

export function newCamera(width: number, height: number, x: number, y: number, r: number, zoom: number, fov: number, offset: number): Camera;
export function drawPlane(camera: Camera, image: Drawable, ox: number, oy: number, sx: number, sy: number, wrap: boolean): void;
export function placeSprite( camera: Camera, image: Drawable, quad: Quad, x: number, y: number, r: number, sx: number, sy: number, ox?: number, oy?: number, kx?: number, ky?: number): void;
export function renderSprites(camera: Camera): void;
/** @tupleReturn */
export function toScreen(camera: Camera, x: number, y: number): [number, number];
/** @tupleReturn */
export function toWorld(camera: Camera, x: number, y: number): [number, number];
