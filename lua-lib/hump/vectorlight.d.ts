/** @noSelfInFile */

export function str(x: number, y: number): string;

/** @tupleReturn */
export function mul(s: number, x: number, y: number): [number, number];

/** @tupleReturn */
export function div(s: number, x: number, y: number): [number, number];

/** @tupleReturn */
export function add(x1: number, y1: number, x2: number, y2: number): [number, number];

/** @tupleReturn */
export function sub(x1: number, y1: number, x2: number, y2: number): [number, number];

/** @tupleReturn */
export function permul(x1: number, y1: number, x2: number, y2: number): [number, number];

export function dot(x1: number, y1: number, x2: number, y2: number): number;

export function det(x1: number, y1: number, x2: number, y2: number): number;

export function eq(x1: number, y1: number, x2: number, y2: number): boolean;

export function lt(x1: number, y1: number, x2: number, y2: number): boolean;

export function le(x1: number, y1: number, x2: number, y2: number): boolean;

export function len2(x: number, y: number): number;

export function len(x: number, y: number): number;

/** @tupleReturn */
export function fromPolar(angle: number, radius: number): [number, number];

/** @tupleReturn */
export function randomDirection(len_min: number, len_max: number): [number, number];

/** @tupleReturn */
export function toPolar(x: number, y: number): [number, number];

export function dist2(x1: number, y1: number, x2: number, y2: number): number;

export function dist(x1: number, y1: number, x2: number, y2: number): number;

/** @tupleReturn */
export function normalize(x: number, y: number): [number, number];

/** @tupleReturn */
export function rotate(phi: number, x: number, y: number): [number, number];

/** @tupleReturn */
export function perpendicular(x: number, y: number): [number, number];

/** @tupleReturn */
export function project(x: number, y: number, u: number, v: number): [number, number];

/** @tupleReturn */
export function mirror(x: number, y: number, u: number, v: number): [number, number];

/** @tupleReturn */
export function trim(maxLen: number, x: number, y: number): [number, number];

export function angleTo(x: number, y: number, u: number, v: number): number;
