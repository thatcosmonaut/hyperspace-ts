/** @noSelfInFile */

export function linear(t: number, b: number, c: number, d: number): number;

export function inQuad(t: number, b: number, c: number, d: number): number;
export function outQuad(t: number, b: number, c: number, d: number): number;
export function inOutQuad(t: number, b: number, c: number, d: number): number;
export function outInQuad(t: number, b: number, c: number, d: number): number;

export function inCubic(t: number, b: number, c: number, d: number): number;
export function outCubic(t: number, b: number, c: number, d: number): number;
export function inOutCubic(t: number, b: number, c: number, d: number): number;
export function outInCubic(t: number, b: number, c: number, d: number): number

export function inQuart(t: number, b: number, c: number, d: number): number;
export function outQuart(t: number, b: number, c: number, d: number): number;
export function inOutQuart(t: number, b: number, c: number, d: number): number;
export function outInQuart(t: number, b: number, c: number, d: number): number;

export function inQuint(t: number, b: number, c: number, d: number): number;
export function outQuint(t: number, b: number, c: number, d: number): number;
export function inOutQuint(t: number, b: number, c: number, d: number): number;
export function outInQuint(t: number, b: number, c: number, d: number): number;

export function inSine(t: number, b: number, c: number, d: number): number;
export function outSine(t: number, b: number, c: number, d: number): number;
export function inOutSine(t: number, b: number, c: number, d: number): number;
export function outInSine(t: number, b: number, c: number, d: number): number;

export function inExpo(t: number, b: number, c: number, d: number): number;
export function outExpo(t: number, b: number, c: number, d: number): number;
export function inOutExpo(t: number, b: number, c: number, d: number): number;
export function outInExpo(t: number, b: number, c: number, d: number): number;

export function inCirc(t: number, b: number, c: number, d: number): number;
export function outCirc(t: number, b: number, c: number, d: number): number;
export function inOutCirc(t: number, b: number, c: number, d: number): number;
export function outInCirc(t: number, b: number, c: number, d: number): number;

export function inElastic(t: number, b: number, c: number, d: number, a: number, p: number): number;
export function outElastic(t: number, b: number, c: number, d: number, a: number, p: number): number;
export function inOutElastic(t: number, b: number, c: number, d: number, a: number, p: number): number;
export function outInElastic(t: number, b: number, c: number, d: number, a: number, p: number): number;

export function inBack(t: number, b: number, c: number, d: number, s: number): number;
export function outBack(t: number, b: number, c: number, d: number, s: number): number;
export function inOutBack(t: number, b: number, c: number, d: number, s: number): number;
export function outInBack(t: number, b: number, c: number, d: number, s: number): number;

export function outBounce(t: number, b: number, c: number, d: number, a: number, p: number): number;
export function inBounce(t: number, b: number, c: number, d: number, a: number, p: number): number;
export function inOutBounce(t: number, b: number, c: number, d: number, a: number, p: number): number;
export function outInBounce(t: number, b: number, c: number, d: number, a: number, p: number): number;
