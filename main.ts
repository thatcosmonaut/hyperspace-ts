declare global {let PROF_CAPTURE: boolean; }
PROF_CAPTURE = true;

import * as jprof from "encompass-jprof";
import { Hyperspace } from "hyperspace/hyperspace";

let hyperspace: Hyperspace;

love.load = () => {
    love.window.setMode(1280, 720, {vsync: true, msaa: 2});
    love.math.setRandomSeed(os.time());
    love.mouse.setVisible(false);

    hyperspace = new Hyperspace();
    hyperspace.load();
};

love.update = (dt) => {
    hyperspace.update(dt);
};

love.draw = () => {
    hyperspace.draw();

    love.graphics.setBlendMode("alpha");
    love.graphics.setColor(1, 1, 1, 1);
    love.graphics.print("Current FPS: " + tostring(love.timer.getFPS()), 10, 10);
};

love.quit = () => {
    jprof.write("prof.mpack");
    return false;
};
