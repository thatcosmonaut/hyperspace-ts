export abstract class GameState {
    public abstract enter(): void;
    public abstract exit(): void;

    public abstract update(dt: number): void;
    public abstract draw(): void;
}
