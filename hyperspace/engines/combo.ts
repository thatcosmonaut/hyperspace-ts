import { Engine, Mutates, Reads } from "encompass-ecs";
import { ComboCounterComponent } from "hyperspace/components/combo_counter";
import { ComboIncreaseMessage } from "hyperspace/messages/combo_increase";
import { ComboTickMessage } from "hyperspace/messages/combo_tick";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";

@Reads(TimeDilationBroadcast, ComboIncreaseMessage, ComboTickMessage)
@Mutates(ComboCounterComponent)
export class ComboEngine extends Engine {
    public update(dt: number) {
        if (this.read_messages(ComboTickMessage).size === 0) { return; }

        const time_dilation_broadcasts = this.read_messages(TimeDilationBroadcast);
        for (const broadcast of time_dilation_broadcasts.values()) {
            dt *= broadcast.factor;
        }

        for (const combo_component of this.read_components_mutable(ComboCounterComponent).values()) {
            combo_component.time_remaining -= dt;

            for (const combo_increase_message of this.read_messages(ComboIncreaseMessage).values()) {
                combo_component.count += combo_increase_message.amount;
                combo_component.time_remaining = combo_component.combo_window;
            }

            combo_component.percentage_time_remaining = math.max(
                combo_component.time_remaining / combo_component.combo_window,
                0,
            );

            if (combo_component.time_remaining < 0) {
                combo_component.count = 0;
            }
        }
    }
}
