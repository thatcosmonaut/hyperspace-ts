import { Emits, Engine } from "encompass-ecs";
import { GCOptimizedMap } from "tstl-gc-optimized-collections";
import { ShipBoostMessage } from "../messages/ship_boost";
import { ShipTurnMessage } from "../messages/ship_turn";
import { ShootMessage } from "../messages/shoot";

enum KeyState {
    Up,
    Pressed,
    Down,
    Released,
}

@Emits(ShootMessage, ShipBoostMessage, ShipTurnMessage)
export class InputHandlerEngine extends Engine {
    private key_states = new GCOptimizedMap<KeyConstant, KeyState>();

    public initialize() {
        this.register_key("z");
        this.register_key("up");
        this.register_key("down");
        this.register_key("left");
        this.register_key("right");
    }

    public update() {
        for (const [key, key_state] of this.key_states.entries()) {
            const is_down = love.keyboard.isDown(key);

            switch (key_state) {
                case KeyState.Up:
                    if (is_down) {
                        this.key_states.set(key, KeyState.Pressed);
                    }
                    break;

                case KeyState.Pressed:
                    if (is_down) {
                        this.key_states.set(key, KeyState.Down);
                    } else {
                        this.key_states.set(key, KeyState.Released);
                    }
                    break;

                case KeyState.Down:
                    if (!is_down) {
                        this.key_states.set(key, KeyState.Released);
                    }
                    break;

                case KeyState.Released:
                    if (is_down) {
                        this.key_states.set(key, KeyState.Down);
                    } else {
                        this.key_states.set(key, KeyState.Up);
                    }
                    break;
            }
        }

        if (this.key_pressed("z")) {
            this.emit_message(ShootMessage);
        }

        if (this.key_down("up")) {
            const boost_message = this.emit_message(ShipBoostMessage);
            boost_message.factor = 1;
        }

        if (this.key_down("left")) {
            const ship_turn_message = this.emit_message(ShipTurnMessage);
            ship_turn_message.amount = -1;
        }

        if (this.key_down("right")) {
            const ship_turn_message = this.emit_message(ShipTurnMessage);
            ship_turn_message.amount = 1;
        }
    }

    protected register_key(key: KeyConstant) {
        this.key_states.set(key, KeyState.Up);
    }

    /**
     * True if the key was just pressed this frame, otherwise false.
     *
     * @param key
     */
    protected key_pressed(key: KeyConstant): boolean {
        return this.key_states.get(key) === KeyState.Pressed;
    }

    /**
     * True if the key is currently being pressed, otherwise false.
     *
     * @param key
     */
    protected key_down(key: KeyConstant): boolean {
        const key_state = this.key_states.get(key);
        return key_state === KeyState.Pressed || key_state === KeyState.Down;
    }

    /**
     * True if the key was just released this frame, otherwise false.
     *
     * @param key
     */
    protected key_released(key: KeyConstant) {
        return this.key_states.get(key) === KeyState.Released;
    }
}
