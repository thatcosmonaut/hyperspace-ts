import { Emits, Engine, Mutates, Reads } from "encompass-ecs";
import { RespawnShipTimerComponent } from "hyperspace/components/timers/respawn_ship";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";
import { RespawnShipTimerMessage } from "hyperspace/messages/respawn_ship_timer";
import { ShipPortalSpawnMessage } from "hyperspace/messages/spawn/ship_portal";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";

@Reads(RespawnShipTimerMessage, TimeDilationBroadcast)
@Emits(ShipPortalSpawnMessage, DestroyEntityMessage)
@Mutates(RespawnShipTimerComponent)
export class RespawnShipTimerEngine extends Engine {
    public update(dt: number) {
        for (const time_dilation_broadcast of this.read_messages(TimeDilationBroadcast).values()) {
            dt *= time_dilation_broadcast.factor;
        }

        const respawn_ship_timer_components = this.read_components_mutable(RespawnShipTimerComponent);
        for (const component of respawn_ship_timer_components.values()) {
            component.time -= dt;

            if (component.time <= 0) {
                const ship_portal_spawn_message = this.emit_message(ShipPortalSpawnMessage);
                ship_portal_spawn_message.time = 3;
                ship_portal_spawn_message.x = 640;
                ship_portal_spawn_message.y = 360;

                this.emit_entity_message(DestroyEntityMessage, this.get_entity(component.entity_id)!);
            }
        }

        const respawn_ship_timer_messages = this.read_messages(RespawnShipTimerMessage);
        for (const message of respawn_ship_timer_messages.values()) {
            const entity = this.create_entity();
            const component = entity.add_component(RespawnShipTimerComponent);
            component.player_index = message.player_index;
            component.time = message.time;
        }
    }
}
