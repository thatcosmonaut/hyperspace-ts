import { Emits, Engine, Reads } from "encompass-ecs";
import { AsteroidAsteroidCollisionMessage } from "hyperspace/messages/collision_messages/asteroid_asteroid";
import { ShockwaveSpawnMessage } from "hyperspace/messages/spawn/shockwave";

@Reads(AsteroidAsteroidCollisionMessage)
@Emits(ShockwaveSpawnMessage)
export class AsteroidAsteroidCollisionResolver extends Engine {
    public update(dt: number) {
        for (const message of this.read_messages(AsteroidAsteroidCollisionMessage).values()) {
            const shockwave_message = this.emit_message(ShockwaveSpawnMessage);

            shockwave_message.x = message.collision_data.x1;
            shockwave_message.y = message.collision_data.y1;
            shockwave_message.size = 10;
        }
    }
}
