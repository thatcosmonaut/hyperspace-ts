import { Emits, Engine, Reads } from "encompass-ecs";
import { AsteroidSizeComponent } from "hyperspace/components/asteroid_size";
import { ColorComponent } from "hyperspace/components/color";
import { AsteroidSplitSpawnMessage } from "hyperspace/messages/asteroid_split_spawn_message";
import { AsteroidBulletCollisionMessage } from "hyperspace/messages/collision_messages/asteroid_bullet";
import { ComboIncreaseMessage } from "hyperspace/messages/combo_increase";
import { EmitExplodeParticleBroadcast } from "hyperspace/messages/emit_explode_particle";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";
import { ShockwaveSpawnMessage } from "hyperspace/messages/spawn/shockwave";
import { SoundSpawnMessage } from "hyperspace/messages/spawn/sound";

@Reads(AsteroidBulletCollisionMessage)
@Emits(
    AsteroidSplitSpawnMessage,
    ShockwaveSpawnMessage,
    SoundSpawnMessage,
    DestroyEntityMessage,
    EmitExplodeParticleBroadcast,
    ComboIncreaseMessage,
)
export class AsteroidBulletCollisionResolver extends Engine {
    public update(dt: number) {
        for (const message of this.read_messages(
            AsteroidBulletCollisionMessage,
        ).values()) {
            const asteroid_split_spawn_message = this.emit_message(AsteroidSplitSpawnMessage);
            asteroid_split_spawn_message.asteroid_entity = message.asteroid_entity;

            const combo_increase_message = this.emit_message(ComboIncreaseMessage);
            combo_increase_message.amount = 1;

            const shockwave_message = this.emit_message(ShockwaveSpawnMessage);
            shockwave_message.x = message.collision_data.x1;
            shockwave_message.y = message.collision_data.y1;
            shockwave_message.size = message.asteroid_entity.get_component(
                AsteroidSizeComponent,
            ).size;

            const sound_message = this.emit_message(SoundSpawnMessage);
            sound_message.filename = "hyperspace/assets/sounds/explosion.wav";
            sound_message.pitch_variation = 0.04;

            const color_component = message.asteroid_entity.get_component(ColorComponent);

            const particle_message = this.emit_message(EmitExplodeParticleBroadcast);
            particle_message.r = color_component.r;
            particle_message.g = color_component.g;
            particle_message.b = color_component.b;
            particle_message.x = message.collision_data.x1;
            particle_message.y = message.collision_data.y1;
            particle_message.amount = 400;

            this.emit_entity_message(DestroyEntityMessage, message.asteroid_entity);
            this.emit_entity_message(DestroyEntityMessage, message.bullet_entity);
        }
    }
}
