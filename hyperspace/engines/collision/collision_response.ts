import { Emits, Engine, Message, Reads, Type } from "encompass-ecs";
import { CollisionType } from "hyperspace/components/collision_types";
import { CheckPairHelper } from "hyperspace/helpers/check_pair";
import { CollisionMessage } from "hyperspace/messages/collision_message";
import { AsteroidAsteroidCollisionMessage } from "hyperspace/messages/collision_messages/asteroid_asteroid";
import { AsteroidBulletCollisionMessage } from "hyperspace/messages/collision_messages/asteroid_bullet";
import { AsteroidShipCollisionMessage } from "hyperspace/messages/collision_messages/asteroid_ship";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(CollisionMessage)
@Emits(AsteroidAsteroidCollisionMessage, AsteroidBulletCollisionMessage, AsteroidShipCollisionMessage)
export class CollisionResolver extends Engine {
    public readonly read_message_types = new GCOptimizedSet<Type<Message>>(CollisionMessage);
    public readonly emit_message_types = new GCOptimizedSet<Type<Message>>(
        AsteroidAsteroidCollisionMessage,
        AsteroidBulletCollisionMessage,
        AsteroidShipCollisionMessage,
    );

    public update(dt: number) {
        const collisions = this.read_messages(CollisionMessage);
        for (const collision of collisions.values()) {
            if (
                CheckPairHelper.check_pairs(
                    collision.collision_type_one,
                    collision.collision_type_two,
                    CollisionType.asteroid,
                    CollisionType.ship,
                )
            ) {
                const message = this.emit_message(AsteroidShipCollisionMessage);
                message.asteroid_entity =
                    collision.collision_type_one === CollisionType.asteroid
                        ? collision.entity_one
                        : collision.entity_two;
                message.ship_entity =
                    collision.collision_type_one === CollisionType.ship
                        ? collision.entity_one
                        : collision.entity_two;
                message.collision_data = collision.collision_data;
            } else if (
                CheckPairHelper.check_pairs(
                    collision.collision_type_one,
                    collision.collision_type_two,
                    CollisionType.asteroid,
                    CollisionType.bullet,
                )
            ) {
                const message = this.emit_message(AsteroidBulletCollisionMessage);
                message.asteroid_entity =
                    collision.collision_type_one === CollisionType.asteroid
                        ? collision.entity_one
                        : collision.entity_two;
                message.bullet_entity =
                    collision.collision_type_one === CollisionType.bullet
                        ? collision.entity_one
                        : collision.entity_two;
                message.collision_data = collision.collision_data;
            } else if (
                CheckPairHelper.check_pairs(
                    collision.collision_type_one,
                    collision.collision_type_two,
                    CollisionType.asteroid,
                    CollisionType.asteroid,
                )
            ) {
                const message = this.emit_message(AsteroidAsteroidCollisionMessage);
                message.asteroid_entity_one = collision.entity_one;
                message.asteroid_entity_two = collision.entity_two;
                message.collision_data = collision.collision_data;
            }
        }
    }
}
