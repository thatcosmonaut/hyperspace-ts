import { Emits, Engine, Message, Reads, Type } from "encompass-ecs";
import { AsteroidSizeComponent } from "hyperspace/components/asteroid_size";
import { ColorComponent } from "hyperspace/components/color";
import { PlayerIndexComponent } from "hyperspace/components/player_index";
import { AsteroidSplitSpawnMessage } from "hyperspace/messages/asteroid_split_spawn_message";
import { AsteroidShipCollisionMessage } from "hyperspace/messages/collision_messages/asteroid_ship";
import { EmitExplodeParticleBroadcast } from "hyperspace/messages/emit_explode_particle";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";
import { LifeDecreaseMessage } from "hyperspace/messages/life_decrease";
import { ShipFragmentsSpawnMessage } from "hyperspace/messages/ship_fragments_spawn";
import { ShockwaveSpawnMessage } from "hyperspace/messages/spawn/shockwave";
import { TimeDilationSpawnMessage } from "hyperspace/messages/spawn/time_dilation";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(AsteroidShipCollisionMessage)
@Emits(
    TimeDilationSpawnMessage,
    DestroyEntityMessage,
    AsteroidSplitSpawnMessage,
    ShockwaveSpawnMessage,
    ShipFragmentsSpawnMessage,
    EmitExplodeParticleBroadcast,
    LifeDecreaseMessage,
)
export class AsteroidShipCollisionResolver extends Engine {
    public read_message_types = new GCOptimizedSet<Type<Message>>(AsteroidShipCollisionMessage);
    public emit_message_types = new GCOptimizedSet<Type<Message>>(
        TimeDilationSpawnMessage,
        DestroyEntityMessage,
        AsteroidSplitSpawnMessage,
        ShockwaveSpawnMessage,
        ShipFragmentsSpawnMessage,
        EmitExplodeParticleBroadcast,
        LifeDecreaseMessage,
    );

    public update(dt: number) {
        for (const message of this.read_messages(AsteroidShipCollisionMessage).values()) {
            const time_dilation_message = this.emit_message(TimeDilationSpawnMessage);
            time_dilation_message.ease_in_time = 0.25;
            time_dilation_message.duration = 1.5;
            time_dilation_message.ease_out_time = 1.5;
            time_dilation_message.factor = 0.05;

            const shockwave_message = this.emit_message(ShockwaveSpawnMessage);
            shockwave_message.x = message.collision_data.x1;
            shockwave_message.y = message.collision_data.y1;
            shockwave_message.size = message.asteroid_entity.get_component(
                AsteroidSizeComponent,
            ).size;

            const color_component = message.asteroid_entity.get_component(ColorComponent);

            const ship_particle_message = this.emit_message(EmitExplodeParticleBroadcast);
            ship_particle_message.r = 1;
            ship_particle_message.g = 1;
            ship_particle_message.b = 1;
            ship_particle_message.x = message.collision_data.x1;
            ship_particle_message.y = message.collision_data.y1;
            ship_particle_message.amount = 1000;

            const asteroid_particle_message = this.emit_message(EmitExplodeParticleBroadcast);
            asteroid_particle_message.r = color_component.r;
            asteroid_particle_message.g = color_component.g;
            asteroid_particle_message.b = color_component.b;
            asteroid_particle_message.x = message.collision_data.x1;
            asteroid_particle_message.y = message.collision_data.y1;
            asteroid_particle_message.amount = 400;

            this.emit_entity_message(DestroyEntityMessage, message.asteroid_entity);
            this.emit_entity_message(DestroyEntityMessage, message.ship_entity);

            const asteroid_split_spawn_message = this.emit_message(AsteroidSplitSpawnMessage);
            asteroid_split_spawn_message.asteroid_entity = message.asteroid_entity;

            const ship_fragments_spawn_message = this.emit_message(ShipFragmentsSpawnMessage);
            ship_fragments_spawn_message.ship_entity = message.ship_entity;

            const life_decrease_message = this.emit_message(LifeDecreaseMessage);
            life_decrease_message.player_index = message.ship_entity.get_component(PlayerIndexComponent).player_index;
            life_decrease_message.amount = 1;
        }
    }
}
