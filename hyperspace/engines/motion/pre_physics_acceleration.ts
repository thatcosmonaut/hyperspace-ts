import { Emits, Engine, Reads } from "encompass-ecs";
import { PhysicsComponent } from "hyperspace/components/physics";
import { UpdatePhysicsVelocityMessage } from "hyperspace/messages/component/update_physics_velocity";
import { UpdateVelocityMessage } from "hyperspace/messages/component/update_velocity";
import { UpdatePostPhysicsVelocityMessage } from "hyperspace/messages/component/update_post_physics_velocity";

@Reads(UpdateVelocityMessage)
@Emits(UpdatePhysicsVelocityMessage, UpdatePostPhysicsVelocityMessage)
export class PrePhysicsAccelerationEngine extends Engine {
    public update() {
        const acceleration_messages = this.read_messages(UpdateVelocityMessage);

        for (const message of acceleration_messages.values()) {
            const entity = this.get_entity(message.component.entity_id)!;

            const update_post_physics_velocity_message = this.emit_component_message(UpdatePostPhysicsVelocityMessage, message.component);
            update_post_physics_velocity_message.x_delta = message.x_delta;
            update_post_physics_velocity_message.y_delta = message.y_delta;
            update_post_physics_velocity_message.angular_delta = message.angular_delta;
            update_post_physics_velocity_message.instant_thrust = message.instant_thrust;
            update_post_physics_velocity_message.instant_torque = message.instant_torque;

            if (entity.has_component(PhysicsComponent)) {
                const update_physics_velocity_message = this.emit_component_message(UpdatePhysicsVelocityMessage, entity.get_component(PhysicsComponent));
                update_physics_velocity_message.x_thrust = message.x_delta;
                update_physics_velocity_message.y_thrust = message.y_delta;
                update_physics_velocity_message.torque = message.angular_delta;
                update_physics_velocity_message.instant_thrust = message.instant_thrust;
                update_physics_velocity_message.instant_torque = message.instant_torque;
            }
        }
    }
}
