import { Emits, Engine, Mutates, Reads } from "encompass-ecs";
import { TransformComponent } from "hyperspace/components/transform";
import { VelocityComponent } from "hyperspace/components/velocity";
import { MathHelper } from "hyperspace/helpers/math";
import { UpdatePostPhysicsTransformMessage } from "hyperspace/messages/component/update_post_physics_transform";
import { UpdatePostPhysicsVelocityMessage } from "hyperspace/messages/component/update_post_physics_velocity";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";
import { trim } from "lua-lib/hump/vectorlight";

@Reads(UpdatePostPhysicsVelocityMessage, TimeDilationBroadcast)
@Emits(UpdatePostPhysicsTransformMessage)
@Mutates(VelocityComponent)
export class VelocityUpdateEngine extends Engine {
    public update(dt: number) {
        const velocity_components = this.read_components(VelocityComponent);

        const time_dilation_broadcasts = this.read_messages(TimeDilationBroadcast);

        for (const time_dilation_broadcast of time_dilation_broadcasts.values()) {
            dt *= time_dilation_broadcast.factor;
        }

        for (const velocity_component of velocity_components.values()) {
            const mutable_velocity_component = this.make_mutable(velocity_component);
            const messages = this.read_messages(UpdatePostPhysicsVelocityMessage);
            let x_velocity = velocity_component.x_linear;
            let y_velocity = velocity_component.y_linear;
            let angular = velocity_component.angular;

            for (const message of messages.values()) {
                if (message.component === velocity_component) {
                    const instant_thrust_or_dt = message.instant_thrust ? 1 : dt;
                    const instant_torque_or_dt = message.instant_torque ? 1 : dt;
                    const x_thrust_adjusted = message.x_delta * instant_thrust_or_dt;
                    const y_thrust_adjusted = message.y_delta * instant_thrust_or_dt;
                    x_velocity += x_thrust_adjusted;
                    y_velocity += y_thrust_adjusted;
                    angular += message.angular_delta * instant_torque_or_dt;
                }
            }

            [x_velocity, y_velocity] = trim(
                velocity_component.max_linear,
                x_velocity,
                y_velocity,
            );

            angular = MathHelper.clamp(
                angular,
                -velocity_component.max_angular,
                velocity_component.max_angular,
            );

            mutable_velocity_component.x_linear = x_velocity;
            mutable_velocity_component.y_linear = y_velocity;
            mutable_velocity_component.angular = angular;

            const entity = this.get_entity(velocity_component.entity_id)!;
            if (entity.has_component(TransformComponent)) {
                const update_transform_message = this.emit_component_message(UpdatePostPhysicsTransformMessage, entity.get_component(TransformComponent));
                update_transform_message.x_delta = x_velocity;
                update_transform_message.y_delta = y_velocity;
                update_transform_message.rotation_delta = angular;
                update_transform_message.instant_linear = false;
                update_transform_message.instant_angular = false;
            }
        }
    }
}
