import { Emits, Engine, Reads } from "encompass-ecs";
import { PhysicsComponent } from "hyperspace/components/physics";
import { UpdatePhysicsTransformMessage } from "hyperspace/messages/component/update_physics_transform";
import { UpdatePostPhysicsTransformMessage } from "hyperspace/messages/component/update_post_physics_transform";
import { UpdateTransformMessage } from "hyperspace/messages/component/update_transform";

@Reads(UpdateTransformMessage)
@Emits(UpdatePhysicsTransformMessage, UpdatePostPhysicsTransformMessage)
export class PrePhysicsMotionEngine extends Engine {
    public update() {
        const update_transform_messages = this.read_messages(UpdateTransformMessage);

        for (const message of update_transform_messages.values()) {
            const entity = this.get_entity(message.component.entity_id)!;

            const update_post_physics_transform_message = this.emit_component_message(UpdatePostPhysicsTransformMessage, message.component);
            update_post_physics_transform_message.x_delta = message.x_delta;
            update_post_physics_transform_message.y_delta = message.y_delta;
            update_post_physics_transform_message.rotation_delta = message.rotation_delta;
            update_post_physics_transform_message.instant_linear = message.instant_linear;
            update_post_physics_transform_message.instant_angular = message.instant_angular;

            if (entity.has_component(PhysicsComponent)) {
                const update_physics_transform_message = this.emit_component_message(UpdatePhysicsTransformMessage, entity.get_component(PhysicsComponent));
                update_physics_transform_message.x_delta = message.x_delta;
                update_physics_transform_message.y_delta = message.y_delta;
                update_physics_transform_message.rotation_delta = message.rotation_delta;
                update_physics_transform_message.instant_linear = message.instant_linear;
                update_physics_transform_message.instant_angular = message.instant_angular;
            }
        }
    }
}
