import { ComponentModifier, Mutates, Reads } from "encompass-ecs";
import { TransformComponent } from "hyperspace/components/transform";
import { UpdatePostPhysicsTransformMessage } from "hyperspace/messages/component/update_post_physics_transform";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";
import { rotate } from "lua-lib/hump/vectorlight";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(UpdatePostPhysicsTransformMessage, TimeDilationBroadcast)
@Mutates(TransformComponent)
export class TransformUpdateEngine extends ComponentModifier {
    public component_message_type = UpdatePostPhysicsTransformMessage;

    protected modify(component: TransformComponent, messages: GCOptimizedSet<UpdatePostPhysicsTransformMessage>, dt: number) {
        let x = component.x;
        let y = component.y;
        let rotation = component.rotation;

        const time_dilation_broadcasts = this.read_messages(TimeDilationBroadcast);

        for (const time_dilation_broadcast of time_dilation_broadcasts.values()) {
            dt *= time_dilation_broadcast.factor;
        }

        for (const message of messages.entries()) {
            const instant_linear_or_dt = message.instant_linear ? 1 : dt;
            const instant_angular_or_dt = message.instant_angular ? 1 : dt;
            x += message.x_delta * instant_linear_or_dt;
            y += message.y_delta * instant_linear_or_dt;
            rotation += message.rotation_delta * instant_angular_or_dt;
        }

        if (component.screen_wrap) {
            x %= love.graphics.getWidth();
            y %= love.graphics.getHeight();
        }

        component.x = x;
        component.y = y;
        component.rotation = rotation;
        [component.forward_x, component.forward_y] = rotate(rotation, 0, 1);
    }
}
