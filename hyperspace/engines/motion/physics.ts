import { ComponentMessage, Emits, Engine, Entity, Mutates, ObjectPool, Reads } from "encompass-ecs";
import { CollisionData } from "hyperspace/collision_data";
import { CollisionTypesComponent } from "hyperspace/components/collision_types";
import { DrivenByPhysics } from "hyperspace/components/driven_by_physics";
import { PhysicsComponent } from "hyperspace/components/physics";
import { TransformComponent } from "hyperspace/components/transform";
import { MathHelper } from "hyperspace/helpers/math";
import { CollisionMessage } from "hyperspace/messages/collision_message";
import { UpdatePhysicsTransformMessage } from "hyperspace/messages/component/update_physics_transform";
import { UpdatePhysicsVelocityMessage } from "hyperspace/messages/component/update_physics_velocity";
import { UpdatePostPhysicsTransformMessage } from "hyperspace/messages/component/update_post_physics_transform";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";
import * as vector from "lua-lib/hump/vectorlight";
import { GCOptimizedMap, GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(
    UpdatePhysicsTransformMessage,
    UpdatePhysicsVelocityMessage,
    TimeDilationBroadcast,
)
@Emits(CollisionMessage, UpdatePostPhysicsTransformMessage)
@Mutates(PhysicsComponent)
export class PhysicsEngine extends Engine {
    private entity_ids = new GCOptimizedSet<number>();
    private entity_id_to_update_transform_messages_map = new GCOptimizedMap<number, GCOptimizedSet<UpdatePhysicsTransformMessage>>();
    private entity_id_to_update_velocity_messages_map = new GCOptimizedMap<number, GCOptimizedSet<UpdatePhysicsVelocityMessage>>();

    private contacts_this_frame = new GCOptimizedMap<number, GCOptimizedMap<number, CollisionData>>();
    private collision_data_pool = new ObjectPool<CollisionData>(CollisionData);

    private physics_world: World;

    public initialize(physics_world: World) {
        physics_world.setCallbacks((a: Fixture, b: Fixture, coll: Contact) => {
            const a_entity = a.getUserData() as Entity;
            const b_entity = b.getUserData() as Entity;

            if (this.check_contact(a_entity.id, b_entity.id)) { return; }

            this.store_contact(a_entity.id, b_entity.id, coll);
        });

        this.physics_world = physics_world;
    }

    public update(dt: number) {
        this.clear_contacts();
        this.clear_entity_data();

        for (const time_dilation_message of this.read_messages(TimeDilationBroadcast).values()) {
            dt *= time_dilation_message.factor;
        }

        this.retrieve_and_map_update_velocity_messages_by_entity();
        this.retrieve_and_map_update_transform_messages_by_entity();

        const physics_components = this.read_components(PhysicsComponent);

        for (const physics_component of physics_components.values()) {
            const entity_id = physics_component.entity_id;

            const update_velocity_messages = this.entity_id_to_update_velocity_messages_map.get(entity_id);
            const update_transform_messages = this.entity_id_to_update_transform_messages_map.get(entity_id);

            let [x_velocity, y_velocity] = physics_component.body.getLinearVelocity();
            let angular_velocity = physics_component.body.getAngularVelocity();

            if (update_velocity_messages !== undefined) {
                for (const message of update_velocity_messages.entries()) {
                    const instant_thrust_or_dt = message.instant_thrust ? 1 : dt;
                    const instant_torque_or_dt = message.instant_torque ? 1 : dt;
                    const x_thrust_adjusted = message.x_thrust * instant_thrust_or_dt;
                    const y_thrust_adjusted = message.y_thrust * instant_thrust_or_dt;
                    x_velocity += x_thrust_adjusted;
                    y_velocity += y_thrust_adjusted;
                    angular_velocity += message.torque * instant_torque_or_dt;
                }
            }

            [x_velocity, y_velocity] = vector.trim(
                physics_component.max_linear_speed,
                x_velocity,
                y_velocity,
            );

            angular_velocity = MathHelper.clamp(
                angular_velocity,
                -physics_component.max_angular_velocity,
                physics_component.max_angular_velocity,
            );

            physics_component.body.setLinearVelocity(x_velocity, y_velocity);
            physics_component.body.setAngularVelocity(angular_velocity);

            let x = physics_component.body.getX();
            let y = physics_component.body.getY();
            let r = physics_component.body.getAngle();

            if (update_transform_messages !== undefined) {
                for (const message of update_transform_messages.entries()) {
                    const instant_linear_or_dt = message.instant_linear ? 1 : dt;
                    const instant_angular_or_dt = message.instant_angular ? 1 : dt;
                    x += x_velocity * instant_linear_or_dt;
                    y += y_velocity * instant_linear_or_dt;
                    r += angular_velocity * instant_angular_or_dt;
                }
            }

            if (physics_component.screen_wrap) {
                x %= love.graphics.getWidth();
                y %= love.graphics.getHeight();
            }

            physics_component.body.setX(x);
            physics_component.body.setY(y);
            physics_component.body.setAngle(r);
        }

        this.physics_world.update(dt);

        for (const physics_component of physics_components.values()) {
            const entity_id = physics_component.entity_id;
            const entity = this.get_entity(entity_id)!;

            if (entity.has_component(DrivenByPhysics)) {
                const transform_component = entity.get_component(TransformComponent);

                const update_velocity_message = this.emit_component_message(UpdatePostPhysicsTransformMessage, transform_component);
                update_velocity_message.x_delta = physics_component.body.getX() - transform_component.x;
                update_velocity_message.y_delta = physics_component.body.getY() - transform_component.y;
                update_velocity_message.rotation_delta = physics_component.body.getAngle() - transform_component.rotation;
                update_velocity_message.instant_linear = true;
                update_velocity_message.instant_angular = true;
            }
        }

        this.handle_collisions();
    }

    private handle_collisions() {
        for (const [a_entity_id, inner_map] of this.contacts_this_frame.entries()) {
            for (const [b_entity_id, coll_data] of inner_map.entries()) {
                if (coll_data.active) {
                    const a_entity = this.get_entity(a_entity_id)!;
                    const b_entity = this.get_entity(b_entity_id)!;
                    if (a_entity.has_component(CollisionTypesComponent)) {
                        if (b_entity.has_component(CollisionTypesComponent)) {
                            for (const a_collision_type of a_entity.get_component(CollisionTypesComponent)!.collision_types) {
                                for (const b_collision_type of b_entity.get_component(CollisionTypesComponent)!.collision_types) {
                                    const collision_message = this.emit_message(CollisionMessage);
                                    collision_message.entity_one = a_entity;
                                    collision_message.entity_two = b_entity;
                                    collision_message.collision_type_one = a_collision_type;
                                    collision_message.collision_type_two = b_collision_type;
                                    collision_message.collision_data = coll_data;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /** @tupleReturn */
    private order_entities(a: number, b: number): [number, number] {
        let first: number;
        let second: number;

        if (a < b) {
            first = a;
            second = b;
        } else {
            first = b;
            second = a;
        }

        return [first, second];
    }

    private check_contact(a: number, b: number): boolean {
        const [first, second] = this.order_entities(a, b);

        return this.contacts_this_frame.has(first) && this.contacts_this_frame.get(first)!.has(second);
    }

    private store_contact(a: number, b: number, coll: Contact): void {
        const [first, second] = this.order_entities(a, b);

        if (!this.contacts_this_frame.has(first)) {
            this.contacts_this_frame.set(first, new GCOptimizedMap());
        }

        const coll_data = this.collision_data_pool.obtain()!;
        coll_data.active = true;
        [coll_data.x1, coll_data.y1, coll_data.x2, coll_data.y2] = coll.getPositions();
        coll_data.friction = coll.getFriction();
        [coll_data.x_normal, coll_data.y_normal] = coll.getNormal();
        coll_data.restitution = coll.getRestitution();

        this.contacts_this_frame.get(first)!.set(second, coll_data);
    }

    private clear_contacts() {
        for (const [entity_id, inner_map] of this.contacts_this_frame.entries()) {
            for (const [__, coll_data] of inner_map.entries()) {
                coll_data.active = false;
                this.collision_data_pool.free(coll_data);
            }
            inner_map.clear();
            if (this.get_entity(entity_id) === undefined) {
                this.contacts_this_frame.delete(entity_id);
            }
        }
    }

    private clear_entity_data() {
        for (const [entity_id, update_transform_messages] of this.entity_id_to_update_transform_messages_map.entries()) {
            update_transform_messages.clear();
        }

        for (const [entity_id, update_velocity_messages] of this.entity_id_to_update_velocity_messages_map.entries()) {
            update_velocity_messages.clear();
        }

        for (const entity_id of this.entity_ids.entries()) {
            if (!this.get_entity(entity_id)) {
                this.entity_id_to_update_transform_messages_map.delete(entity_id);
                this.entity_id_to_update_velocity_messages_map.delete(entity_id);
            }
        }

        this.entity_ids.clear();
    }

    private retrieve_and_map_update_transform_messages_by_entity() {
        const update_physics_transform_messages = this.read_messages(UpdatePhysicsTransformMessage);
        for (const update_transform_message of update_physics_transform_messages.values()) {
            this.map_message_by_entity(this.entity_id_to_update_transform_messages_map, update_transform_message);
            this.entity_ids.add(update_transform_message.component.entity_id);
        }
    }

    private retrieve_and_map_update_velocity_messages_by_entity() {
        const update_physics_velocity_messages = this.read_messages(UpdatePhysicsVelocityMessage);
        for (const update_physics_velocity_message of update_physics_velocity_messages.values()) {
            this.map_message_by_entity(this.entity_id_to_update_velocity_messages_map, update_physics_velocity_message);
            this.entity_ids.add(update_physics_velocity_message.component.entity_id);
        }
    }

    private map_message_by_entity<TComponentMessage extends ComponentMessage>(
        map: GCOptimizedMap<number, GCOptimizedSet<TComponentMessage>>,
        message: TComponentMessage,
    ) {
        if (!map.has(message.component.entity_id)) {
            map.set(message.component.entity_id, new GCOptimizedSet<TComponentMessage>());
        }
        map.get(message.component.entity_id)!.add(message);
    }
}
