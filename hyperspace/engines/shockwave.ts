import { Engine, Mutates, Reads } from "encompass-ecs";
import { ShockwaveComponent } from "hyperspace/components/draw/effects/shockwave";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";

@Reads(TimeDilationBroadcast)
@Mutates(ShockwaveComponent)
export class ShockwaveEngine extends Engine {
    public update(dt: number) {
        for (const broadcast of this.read_messages(TimeDilationBroadcast).values()) {
            dt *= broadcast.factor;
        }

        const shockwave_components = this.read_components_mutable(ShockwaveComponent);

        for (const shockwave_component of shockwave_components.values()) {
            shockwave_component.time_elapsed += dt;
            shockwave_component.shader.send("time", shockwave_component.time_elapsed);
        }
    }
}
