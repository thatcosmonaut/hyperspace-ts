import { Emits, Engine, Mutates, Reads } from "encompass-ecs";
import { ShipPortalComponent } from "hyperspace/components/ship_portal";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";
import { ShipSpawnMessage } from "hyperspace/messages/spawn/ship";
import { ShockwaveSpawnMessage } from "hyperspace/messages/spawn/shockwave";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";

@Reads(TimeDilationBroadcast)
@Emits(ShipSpawnMessage, DestroyEntityMessage, ShockwaveSpawnMessage)
@Mutates(ShipPortalComponent)
export class ShipPortalEngine extends Engine {
    public update(dt: number) {
        for (const broadcast of this.read_messages(TimeDilationBroadcast).values()) {
            dt *= broadcast.factor;
        }

        const ship_portal_components = this.read_components_mutable(ShipPortalComponent);

        for (const component of ship_portal_components.values()) {
            component.time -= dt;

            if (component.time <= 0) {
                const message = this.emit_message(ShipSpawnMessage);
                message.player_index = 0;
                message.x_position = component.x;
                message.y_position = component.y;
                message.rotation = 0;
                message.x_velocity = 0;
                message.y_velocity = 0;
                message.angular_velocity = 0;
                message.max_linear_speed = 1000;
                message.max_angular_velocity = math.huge;

                const shockwave_message = this.emit_message(ShockwaveSpawnMessage);
                shockwave_message.x = component.x;
                shockwave_message.y = component.y;
                shockwave_message.size = 20;

                this.emit_entity_message(DestroyEntityMessage, this.get_entity(component.entity_id)!);
            }
        }
    }
}
