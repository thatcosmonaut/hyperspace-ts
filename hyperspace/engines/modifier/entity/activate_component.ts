import { Entity, EntityModifier, Reads } from "encompass-ecs";
import { ActivateComponentMessage } from "hyperspace/messages/entity/activate_component";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(ActivateComponentMessage)
export class ActivateComponentModifier extends EntityModifier {
    protected readonly entity_message_type = ActivateComponentMessage;

    protected modify(entity: Entity, messages: GCOptimizedSet<ActivateComponentMessage>) {
        for (const message of messages.entries()) {
            entity.activate_component(message.component);
        }
    }
}
