import { Component, Entity, EntityModifier, Reads } from "encompass-ecs";
import { AddComponentMessage } from "hyperspace/messages/entity/add_component";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

type MessageSet = GCOptimizedSet<AddComponentMessage<Component>>;

@Reads(AddComponentMessage)
export class AddComponentModifier extends EntityModifier {
    protected readonly entity_message_type = AddComponentMessage;

    protected modify(entity: Entity, messages: MessageSet) {
        for (const message of messages.entries()) {
            entity.add_component(message.component_to_add, message.args);
        }
    }
}
