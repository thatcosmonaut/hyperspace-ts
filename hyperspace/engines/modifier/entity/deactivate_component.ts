import { Entity, EntityModifier, Reads } from "encompass-ecs";
import { DeactivateComponentMessage } from "hyperspace/messages/entity/deactivate_component";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(DeactivateComponentMessage)
export class DeactivateComponentModifier extends EntityModifier {
    protected readonly entity_message_type = DeactivateComponentMessage;

    protected modify(entity: Entity, messages: GCOptimizedSet<DeactivateComponentMessage>) {
        for (const message of messages.entries()) {
            entity.deactivate_component(message.component);
        }
    }
}
