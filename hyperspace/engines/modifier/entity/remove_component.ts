import { Entity, EntityModifier, Reads } from "encompass-ecs";
import { RemoveComponentMessage } from "hyperspace/messages/entity/remove_component";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(RemoveComponentMessage)
export class RemoveComponentModifier extends EntityModifier {
    protected readonly entity_message_type = RemoveComponentMessage;

    protected modify(entity: Entity, messages: GCOptimizedSet<RemoveComponentMessage>, dt: number) {
        for (const message of messages.entries()) {
            entity.remove_component(message.component_to_remove);
        }
    }
}
