import { Entity, EntityModifier, Reads } from "encompass-ecs";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";

@Reads(DestroyEntityMessage)
export class DestroyEntityModifier extends EntityModifier {
    protected readonly entity_message_type = DestroyEntityMessage;

    protected modify(entity: Entity) {
        entity.destroy();
    }
}
