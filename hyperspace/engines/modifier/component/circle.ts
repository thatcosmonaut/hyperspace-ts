import { ComponentModifier, Mutates, Reads } from "encompass-ecs";
import { CircleDrawComponent } from "hyperspace/components/draw/circle";
import { CircleMessage } from "hyperspace/messages/component/circle";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(CircleMessage)
@Mutates(CircleDrawComponent)
export class CircleModifier extends ComponentModifier {
    protected readonly component_message_type = CircleMessage;

    protected modify(circle_component: CircleDrawComponent, messages: GCOptimizedSet<CircleMessage>, dt: number) {
        for (const message of messages.entries()) {
            circle_component.radius += message.radius_delta;
        }
    }
}
