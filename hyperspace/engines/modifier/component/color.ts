import { ComponentModifier, Mutates, Reads } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";
import { ChangeColorMessage } from "hyperspace/messages/change_color";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(ChangeColorMessage)
@Mutates(ColorComponent)
export class ColorEngine extends ComponentModifier {
    public component_message_type = ChangeColorMessage;

    public modify(component: ColorComponent, messages: GCOptimizedSet<ChangeColorMessage>, dt: number) {
        for (const message of messages.entries()) {
            if (message.r !== undefined) {
                component.r = message.r;
            }
            if (message.g !== undefined) {
                component.g = message.g;
            }
            if (message.b !== undefined) {
                component.b = message.b;
            }
            if (message.a !== undefined) {
                component.a = message.a;
            }
        }
    }
}
