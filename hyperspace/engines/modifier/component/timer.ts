import { ComponentModifier, Mutates, Reads } from "encompass-ecs";
import { TimerComponent } from "hyperspace/components/timer";
import { ActivateComponentTimerComponent } from "hyperspace/components/timers/activate_component";
import { AddComponentTimerComponent } from "hyperspace/components/timers/add_component";
import { DestroyTimer } from "hyperspace/components/timers/destroy";
import { FadeTimer } from "hyperspace/components/timers/fade";
import { TimerMessage } from "hyperspace/messages/component/timer";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(TimerMessage, TimeDilationBroadcast)
@Mutates(ActivateComponentTimerComponent, AddComponentTimerComponent, DestroyTimer, FadeTimer)
export class TimerModifier extends ComponentModifier {
    protected readonly component_message_type = TimerMessage;

    protected modify(component: TimerComponent, _messages: GCOptimizedSet<TimerMessage>, dt: number) {
        for (const broadcast of this.read_messages(TimeDilationBroadcast).values()) {
            dt *= broadcast.factor;
        }

        component.time -= dt;
    }
}
