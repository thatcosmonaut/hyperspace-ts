import { ComponentModifier, Mutates, Reads } from "encompass-ecs";
import { SimpleBloomComponent } from "hyperspace/components/draw/effects/simple_bloom";
import { SimpleBloomMessage } from "hyperspace/messages/component/simple_bloom";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(SimpleBloomMessage)
@Mutates(SimpleBloomComponent)
export class SimpleBloomModifier extends ComponentModifier {
    protected readonly component_message_type = SimpleBloomMessage;

    protected modify(bloom_component: SimpleBloomComponent, messages: GCOptimizedSet<SimpleBloomMessage>, dt: number) {
        for (const message of messages.entries()) {
            bloom_component.power = message.power;
            bloom_component.shader.send("power", message.power);
        }
    }
}
