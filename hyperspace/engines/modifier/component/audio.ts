import { ComponentModifier, Mutates, Reads } from "encompass-ecs";
import { MusicComponent } from "hyperspace/components/music";
import { SoundComponent } from "hyperspace/components/sound";
import { AudioMessage } from "hyperspace/messages/component/audio";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";
import { AudioComponent } from "hyperspace/types/audio_component";
import { GCOptimizedSet } from "tstl-gc-optimized-collections";

@Reads(TimeDilationBroadcast, AudioMessage)
@Mutates(MusicComponent, SoundComponent)
export class AudioModifier extends ComponentModifier {
    protected component_message_type = AudioMessage;

    protected modify(music_component: AudioComponent, messages: GCOptimizedSet<AudioMessage>, dt: number) {
        let factor = 1;
        for (const broadcast of this.read_messages(TimeDilationBroadcast).values()) {
            factor *= broadcast.factor;
        }

        dt *= factor;

        const source = music_component.source;
        source.setPitch(factor);
        music_component.time += dt;
    }
}
