import { Emits, Engine, Reads } from "encompass-ecs";
import { TransformComponent } from "hyperspace/components/transform";
import { VelocityComponent } from "hyperspace/components/velocity";
import { MathHelper } from "hyperspace/helpers/math";
import { ShipFragmentsSpawnMessage } from "hyperspace/messages/ship_fragments_spawn";
import { ShipPieceSpawnMessage } from "hyperspace/messages/spawn/ship_piece";
import * as vector from "lua-lib/hump/vectorlight";

@Reads(ShipFragmentsSpawnMessage)
@Emits(ShipPieceSpawnMessage)
export class ShipFragmentsSpawner extends Engine {
    public update() {
        for (const message of this.read_messages(ShipFragmentsSpawnMessage).values()) {
            const entity = message.ship_entity;

            const transform_component = entity.get_component(TransformComponent);
            const velocity_component = entity.get_component(VelocityComponent);

            let [x_velocity, y_velocity] = vector.mul(
                20,
                ...vector.normalize(
                    ...vector.rotate(
                        math.pi * 0.5,
                        velocity_component.x_linear,
                        velocity_component.y_linear,
                    ),
                ),
            );

            const fragment_spawn_message = this.emit_message(ShipPieceSpawnMessage);
            fragment_spawn_message.x = transform_component.x;
            fragment_spawn_message.y = transform_component.y;
            fragment_spawn_message.rotation = transform_component.rotation + (math.pi * 0.125);
            fragment_spawn_message.x_velocity = x_velocity;
            fragment_spawn_message.y_velocity = y_velocity;
            fragment_spawn_message.angular_velocity = MathHelper.randomFloat(-math.pi, math.pi);
            fragment_spawn_message.length = 16;
            fragment_spawn_message.color = {r: 1, g: 1, b: 1, a: 1};

            [x_velocity, y_velocity] = vector.mul(
                20,
                ...vector.normalize(
                    ...vector.rotate(
                        math.pi * 3 / 2,
                        velocity_component.x_linear,
                        velocity_component.y_linear,
                    ),
                ),
            );

            const fragment_spawn_message_two = this.emit_message(ShipPieceSpawnMessage);
            fragment_spawn_message_two.x = transform_component.x;
            fragment_spawn_message_two.y = transform_component.y;
            fragment_spawn_message_two.rotation = transform_component.rotation - (math.pi * 0.125);
            fragment_spawn_message_two.x_velocity = x_velocity;
            fragment_spawn_message_two.y_velocity = y_velocity;
            fragment_spawn_message_two.angular_velocity = MathHelper.randomFloat(-math.pi, math.pi);
            fragment_spawn_message_two.length = 16;
            fragment_spawn_message_two.color = {r: 1, g: 1, b: 1, a: 1};

            [x_velocity, y_velocity] = vector.mul(20,
                ...vector.normalize(
                    ...vector.rotate(
                        math.pi,
                        velocity_component.x_linear,
                        velocity_component.y_linear,
                    ),
                ),
            );

            const fragment_spawn_message_three = this.emit_message(ShipPieceSpawnMessage);
            fragment_spawn_message_three.x = transform_component.x;
            fragment_spawn_message_three.y = transform_component.y;
            fragment_spawn_message_three.rotation = transform_component.rotation + (math.pi * 0.25);
            fragment_spawn_message_three.x_velocity = x_velocity;
            fragment_spawn_message_three.y_velocity = y_velocity;
            fragment_spawn_message_three.angular_velocity = MathHelper.randomFloat(-math.pi, math.pi);
            fragment_spawn_message_three.length = 8;
            fragment_spawn_message_three.color = {r: 1, g: 1, b: 1, a: 1};
        }
    }
}
