import { Detector, Emits, Entity } from "encompass-ecs";
import { AddComponentTimerComponent } from "hyperspace/components/timers/add_component";
import { TimerMessage } from "hyperspace/messages/component/timer";
import { AddComponentMessage } from "hyperspace/messages/entity/add_component";
import { RemoveComponentMessage } from "hyperspace/messages/entity/remove_component";

@Emits(TimerMessage, RemoveComponentMessage, AddComponentMessage)
export class AddComponentTimerDetector extends Detector {
    public component_types = [ AddComponentTimerComponent ];

    protected detect(entity: Entity) {
        const timer_components = entity.get_components(AddComponentTimerComponent);

        for (const [_, timer_component] of timer_components.entries()) {
            if (timer_component.time <= 0) {
                const remove_component_message = this.emit_entity_message(RemoveComponentMessage, entity);
                remove_component_message.component_to_remove = timer_component;

                const add_component_message = this.emit_entity_message(AddComponentMessage, entity);
                add_component_message.component_to_add = timer_component.component_to_add;
                add_component_message.args = timer_component.args;
            } else {
                this.emit_component_message(TimerMessage, timer_component);
            }
        }
    }
}
