import { Detector, Emits, Entity } from "encompass-ecs";
import { MusicComponent } from "hyperspace/components/music";
import { AudioMessage } from "hyperspace/messages/component/audio";

@Emits(AudioMessage)
export class MusicDetector extends Detector {
    public component_types = [MusicComponent];

    protected detect(entity: Entity) {
        const music_component = entity.get_component(MusicComponent);

        this.emit_component_message(AudioMessage, music_component);
    }
}
