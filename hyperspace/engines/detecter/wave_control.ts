import { Detector, Emits, Entity, Mutates } from "encompass-ecs";
import { AsteroidCounterComponent } from "hyperspace/components/asteroid_counter";
import { AsteroidPortalComponent } from "hyperspace/components/asteroid_portal";
import { WaveControlComponent, WaveState } from "hyperspace/components/wave_control";
import { ComboTickMessage } from "hyperspace/messages/combo_tick";
import { TimerMessage } from "hyperspace/messages/component/timer";
import { AsteroidPortalSpawnMessage } from "hyperspace/messages/spawn/asteroid_portal";
import { SoundSpawnMessage } from "hyperspace/messages/spawn/sound";
import * as vector from "lua-lib/hump/vectorlight";
import { GCOptimizedMap } from "tstl-gc-optimized-collections";

@Emits(TimerMessage, AsteroidPortalSpawnMessage, SoundSpawnMessage, ComboTickMessage)
@Mutates(WaveControlComponent)
export class WaveControlDetector extends Detector {
    public component_types = [ WaveControlComponent ];

    private asteroid_colors = [
        {r: 1, g: 0, b: 37 / 255, a: 1}, // red
        {r: 6 / 255, g: 174 / 255, b: 213 / 255, a: 1}, // blue
        {r: 1, g: 233 / 255, b: 92 / 255, a: 1}, // yellow
        {r: 4 / 255, g: 231 / 255, b: 98 / 255, a: 1}, // green
    ];

    private wave_announce_files = new GCOptimizedMap<number, string>();

    protected detect(entity: Entity, dt: number) {
        const wave_control_component = this.make_mutable(entity.get_component(WaveControlComponent));

        switch (wave_control_component.wave_state) {
            case WaveState.preStart:
                if (wave_control_component.time <= 0) {
                    wave_control_component.wave_state = WaveState.spawning;
                } else {
                    wave_control_component.time -= dt;
                }
                break;

            case WaveState.spawning:
                wave_control_component.wave += 1;
                let i = 0;
                while (i < wave_control_component.wave) {
                    const portal_spawn_message = this.emit_message(AsteroidPortalSpawnMessage);
                    portal_spawn_message.x = love.math.random(128, 1280 - 128);
                    portal_spawn_message.y = love.math.random(128, 720 - 128);
                    portal_spawn_message.color = this.asteroid_colors[love.math.random(this.asteroid_colors.length - 1)];
                    portal_spawn_message.grow_time = 1;
                    portal_spawn_message.hold_time = 1;
                    portal_spawn_message.size = 128;
                    [
                        portal_spawn_message.x_launch_velocity,
                        portal_spawn_message.y_launch_velocity,
                    ] = vector.randomDirection(30, 60);

                    i += 1;
                }

                const wave_announce_message = this.emit_message(SoundSpawnMessage);
                wave_announce_message.filename = this.get_wave_announce_filename(wave_control_component.wave);
                wave_announce_message.pitch_variation = 0;
                wave_control_component.wave_state = WaveState.waitingForSpawn;
                break;

            case WaveState.waitingForSpawn:
                if (this.read_components(AsteroidPortalComponent).size === 0 &&
                    this.read_components(AsteroidCounterComponent).size > 0) {
                    wave_control_component.wave_state = WaveState.inProgress;
                }

                break;

            case WaveState.inProgress:
                if (this.read_components(AsteroidCounterComponent).size === 0) {
                    const applause_message = this.emit_message(SoundSpawnMessage);
                    applause_message.filename = "hyperspace/assets/sounds/applause.wav";
                    applause_message.pitch_variation = 0;
                    wave_control_component.wave_state = WaveState.ending;
                } else {
                    this.emit_message(ComboTickMessage);
                }
                break;

            case WaveState.ending:
                wave_control_component.wave_state = WaveState.preStart;
                wave_control_component.time = 5;
                wave_control_component.asteroid_spawned = false;
                break;
        }
    }

    private get_wave_announce_filename(wave: number): string {
        const filename = this.wave_announce_files.get(wave);
        if (filename === undefined) {
            return "hyperspace/assets/sounds/announcer/wave_begin.wav";
        } else {
            return filename;
        }
    }
}
