import { Detector, Emits, Entity, Reads } from "encompass-ecs";
import { CanShootComponent } from "hyperspace/components/can_shoot";
import { GunComponent } from "hyperspace/components/gun";
import { AddComponentTimerComponent } from "hyperspace/components/timers/add_component";
import { TransformComponent } from "hyperspace/components/transform";
import { AddComponentMessage } from "hyperspace/messages/entity/add_component";
import { RemoveComponentMessage } from "hyperspace/messages/entity/remove_component";
import { ShootMessage } from "hyperspace/messages/shoot";
import { BulletSpawnMessage } from "hyperspace/messages/spawn/bullet";
import { SoundSpawnMessage } from "hyperspace/messages/spawn/sound";
import * as vectorlight from "lua-lib/hump/vectorlight";

const BULLET_SPEED = 900;

@Reads(ShootMessage)
@Emits(AddComponentMessage, BulletSpawnMessage, SoundSpawnMessage, RemoveComponentMessage)
export class ShipShootDetector extends Detector {
    public component_types = [ TransformComponent, GunComponent, CanShootComponent ];

    protected detect(entity: Entity) {
        const transform_component = entity.get_component(TransformComponent);
        const gun_component = entity.get_component(GunComponent);
        const can_shoot_component = entity.get_component(CanShootComponent);

        if (this.read_messages(ShootMessage).size > 0) {
            const [x_pos, y_pos] = vectorlight.add(
                transform_component.x,
                transform_component.y,
                ...vectorlight.mul(
                    12,
                    transform_component.forward_x,
                    transform_component.forward_y,
                ),
            );

            const [x_vel, y_vel] = vectorlight.mul(
                BULLET_SPEED,
                transform_component.forward_x,
                transform_component.forward_y,
            );

            const bullet_spawn_message = this.emit_message(BulletSpawnMessage);
            bullet_spawn_message.x_position = x_pos;
            bullet_spawn_message.y_position = y_pos;
            bullet_spawn_message.x_velocity = x_vel;
            bullet_spawn_message.y_velocity = y_vel;

            const sound_spawn_message = this.emit_message(SoundSpawnMessage);
            sound_spawn_message.filename = "hyperspace/assets/sounds/fire.wav";
            sound_spawn_message.pitch_variation = 0;

            const remove_component_message = this.emit_entity_message(RemoveComponentMessage, entity);
            remove_component_message.component_to_remove = can_shoot_component;

            const add_component_message = this.emit_entity_message(
                AddComponentMessage,
                entity,
            ) as AddComponentMessage<AddComponentTimerComponent<CanShootComponent>>;

            add_component_message.component_to_add = AddComponentTimerComponent;
            add_component_message.args = {
                component_to_add: CanShootComponent,
                time: gun_component.interval,
                args: {},
            };
        }
    }
}
