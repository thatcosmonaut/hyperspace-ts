import { Detector, Emits, Entity } from "encompass-ecs";
import { SimpleBloomComponent } from "hyperspace/components/draw/effects/simple_bloom";
import { MusicComponent } from "hyperspace/components/music";
import { MathHelper } from "hyperspace/helpers/math";
import { SimpleBloomMessage } from "hyperspace/messages/component/simple_bloom";

@Emits(SimpleBloomMessage)
export class BeatBloomDetector extends Detector {
    public component_types = [MusicComponent, SimpleBloomComponent];

    protected detect(entity: Entity) {
        const music_component = entity.get_component(MusicComponent);
        const simple_bloom_component = entity.get_component(SimpleBloomComponent);

        const intensity = this.get_intensity(music_component);

        let power = 0.5 + intensity * math.sin((2 * math.pi) / (1 / music_component.bpm) / 60 * music_component.time);
        power = MathHelper.clamp(power, 0.5, 2.0);

        const simple_bloom_message = this.emit_component_message(SimpleBloomMessage, simple_bloom_component);
        simple_bloom_message.power = power;
    }

    protected get_intensity(music_component: Readonly<MusicComponent>) {
        for (const section of music_component.sections.sections) {
            if (section.time === undefined) {
                return section.intensity;
            } else if (music_component.time < section.time) {
                return section.intensity;
            }
        }

        return music_component.sections.default;
    }
}
