import { Detector, Emits, Entity } from "encompass-ecs";
import { SoundComponent } from "hyperspace/components/sound";
import { AudioMessage } from "hyperspace/messages/component/audio";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";

@Emits(DestroyEntityMessage, AudioMessage)
export class SoundDetector extends Detector {
    public component_types = [ SoundComponent ];

    protected detect(entity: Entity) {
        const sound_component = entity.get_component(SoundComponent);

        if (!sound_component.source.isPlaying()) {
            this.emit_entity_message(DestroyEntityMessage, entity);
        } else {
            this.emit_component_message(AudioMessage, sound_component);
        }
    }
}
