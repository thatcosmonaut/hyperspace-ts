import { Detector, Emits, Entity, Mutates } from "encompass-ecs";
import { TimeDilationComponent } from "hyperspace/components/time_dilation";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";
import * as Easing from "lua-lib/easing";

@Emits(DestroyEntityMessage, TimeDilationBroadcast)
@Mutates(TimeDilationComponent)
export class TimeDilationDetector extends Detector {
    public component_types = [ TimeDilationComponent ];

    protected detect(entity: Entity, dt: number) {
        const time_dilation_component = this.make_mutable(entity.get_component(TimeDilationComponent));
        time_dilation_component.time_elapsed += dt;

        const factor = time_dilation_component.factor;
        const time_elapsed = time_dilation_component.time_elapsed;
        const ease_in_time = time_dilation_component.ease_in_time;
        const duration = time_dilation_component.duration;
        const ease_out_time = time_dilation_component.ease_out_time;

        let calculated_factor = 1;

        if (time_elapsed < ease_in_time) {
            calculated_factor = Easing.inQuad(time_elapsed, 1, factor - 1, ease_in_time);
        } else if (time_elapsed < ease_in_time + duration) {
            calculated_factor = factor;
        } else if (time_elapsed < ease_in_time + duration + ease_out_time) {
            const elapsed_out_time = time_elapsed - ease_in_time - duration;
            calculated_factor = Easing.inQuint(elapsed_out_time, factor, 1 - factor, ease_out_time);
        } else {
            this.emit_entity_message(DestroyEntityMessage, entity);
        }

        const state_message = this.emit_message(TimeDilationBroadcast);
        state_message.factor = calculated_factor;
    }
}
