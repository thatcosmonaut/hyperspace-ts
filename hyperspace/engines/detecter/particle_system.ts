import { Detector, Emits, Entity } from "encompass-ecs";
import { ParticleSystemContainerComponent } from "hyperspace/components/draw/effects/particle_system_container";
import { ParticleSystemContainerMessage } from "hyperspace/messages/component/particle_system_container";

@Emits(ParticleSystemContainerMessage)
export class ParticleSystemDetector extends Detector {
    public component_types = [ ParticleSystemContainerComponent ];

    protected detect(entity: Entity) {
        this.emit_component_message(
            ParticleSystemContainerMessage,
            entity.get_component(ParticleSystemContainerComponent),
        );
    }
}
