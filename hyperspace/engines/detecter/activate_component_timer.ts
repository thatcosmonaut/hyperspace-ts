import { Detector, Emits, Entity } from "encompass-ecs";
import { ActivateComponentTimerComponent } from "hyperspace/components/timers/activate_component";
import { TimerMessage } from "hyperspace/messages/component/timer";
import { ActivateComponentMessage } from "hyperspace/messages/entity/activate_component";
import { RemoveComponentMessage } from "hyperspace/messages/entity/remove_component";

@Emits(TimerMessage, RemoveComponentMessage, ActivateComponentMessage)
export class ActivateComponentTimerDetector extends Detector {
    public component_types = [ ActivateComponentTimerComponent ];

    protected detect(entity: Entity) {
        const timer_components = entity.get_components(ActivateComponentTimerComponent);

        for (const [_, timer_component] of timer_components.entries()) {
            if (timer_component.time <= 0) {
                const remove_component_message = this.emit_entity_message(RemoveComponentMessage, entity);
                remove_component_message.component_to_remove = timer_component;

                const activate_component_message = this.emit_entity_message(ActivateComponentMessage, entity);
                activate_component_message.component = timer_component.component_to_activate;
            } else {
                this.emit_component_message(TimerMessage, timer_component);
            }
        }
    }
}
