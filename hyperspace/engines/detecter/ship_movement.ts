import { Detector, Emits, Reads } from "encompass-ecs";
import { Entity } from "encompass-ecs";
import { TransformComponent } from "hyperspace/components/transform";
import { VelocityComponent } from "hyperspace/components/velocity";
import { UpdateVelocityMessage } from "hyperspace/messages/component/update_velocity";
import { ShipBoostMessage } from "hyperspace/messages/ship_boost";
import { ShipTurnMessage } from "hyperspace/messages/ship_turn";

const THRUST_VALUE = 600;
const ROTATION_SPEED = math.pi * 2;

@Reads(ShipBoostMessage, ShipTurnMessage)
@Emits(UpdateVelocityMessage)
export class ShipMovementDetector extends Detector {
    public component_types = [TransformComponent, VelocityComponent];

    protected detect(entity: Entity) {
        const transform_component = entity.get_component(TransformComponent);
        const velocity_component = entity.get_component(VelocityComponent);

        let turn_value = 0;

        for (const turn_message of this.read_messages(ShipTurnMessage).values()) {
            turn_value += turn_message.amount;
        }

        let x_thrust = 0;
        let y_thrust = 0;

        const boost_messages = this.read_messages(ShipBoostMessage);
        for (const boost_message of boost_messages.values()) {
            const scalar = boost_message.factor * THRUST_VALUE;
            x_thrust = transform_component.forward_x * scalar;
            y_thrust = transform_component.forward_y * scalar;
        }

        const torque = this.axis_input_to_rotation_speed(turn_value) - velocity_component.angular;

        const acceleration_message = this.emit_component_message(UpdateVelocityMessage, velocity_component);
        acceleration_message.x_delta = x_thrust;
        acceleration_message.y_delta = y_thrust;
        acceleration_message.angular_delta = torque;
        acceleration_message.instant_thrust = false;
        acceleration_message.instant_torque = true;
    }

    private axis_input_to_rotation_speed(axis_input: number): number {
        return axis_input * ROTATION_SPEED;
    }

    private bool_to_number(bool: boolean) {
        return bool ? 1 : 0;
    }
}
