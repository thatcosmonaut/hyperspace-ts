import { Detector, Emits, Entity, Mutates, Reads } from "encompass-ecs";
import { AsteroidPortalComponent } from "hyperspace/components/asteroid_portal";
import { ColorComponent } from "hyperspace/components/color";
import { AsteroidFormationComponent } from "hyperspace/components/draw/asteroid_formation";
import { CircleDrawComponent } from "hyperspace/components/draw/circle";
import { TransformComponent } from "hyperspace/components/transform";
import { CircleMessage } from "hyperspace/messages/component/circle";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";
import { AsteroidSpawnMessage } from "hyperspace/messages/spawn/asteroid";
import { ShockwaveSpawnMessage } from "hyperspace/messages/spawn/shockwave";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";
import * as Easing from "lua-lib/easing";

@Emits(CircleMessage, ShockwaveSpawnMessage, AsteroidSpawnMessage, DestroyEntityMessage)
@Reads(TimeDilationBroadcast)
@Mutates(AsteroidPortalComponent, AsteroidFormationComponent)
export class AsteroidPortalDetector extends Detector {
    public component_types = [
        TransformComponent,
        AsteroidPortalComponent,
        AsteroidFormationComponent,
        CircleDrawComponent,
        ColorComponent,
    ];

    protected detect(entity: Entity, dt: number) {
        const transform_component = entity.get_component(TransformComponent);
        const asteroid_portal_component = this.make_mutable(entity.get_component(AsteroidPortalComponent));
        const circle_draw_component = entity.get_component(CircleDrawComponent);
        const color_component = entity.get_component(ColorComponent);
        const asteroid_formation_component = this.make_mutable(entity.get_component(AsteroidFormationComponent));

        const time_dilation_broadcasts = this.read_messages(TimeDilationBroadcast);
        for (const time_dilation_broadcast of time_dilation_broadcasts.values()) {
            dt *= time_dilation_broadcast.factor;
        }

        asteroid_portal_component.time_elapsed += dt;

        if (asteroid_portal_component.time_elapsed < asteroid_portal_component.grow_time) {
            const new_size = Easing.linear(
                asteroid_portal_component.time_elapsed,
                0.01,
                asteroid_portal_component.size - 0.01,
                asteroid_portal_component.grow_time,
            );

            const grow_amount = new_size - circle_draw_component.radius;

            const circle_message = this.emit_component_message(CircleMessage, circle_draw_component);
            circle_message.radius_delta = grow_amount;
        } else if (asteroid_portal_component.time_elapsed < asteroid_portal_component.grow_time + asteroid_portal_component.hold_time) {
            asteroid_formation_component.time_elapsed += dt;
        } else {
            const shockwave_message = this.emit_message(ShockwaveSpawnMessage);
            shockwave_message.x = transform_component.x;
            shockwave_message.y = transform_component.y;
            shockwave_message.size = asteroid_portal_component.size;

            this.emit_entity_message(DestroyEntityMessage, entity);

            const asteroid_spawn_message = this.emit_message(AsteroidSpawnMessage);
            asteroid_spawn_message.x_position = transform_component.x;
            asteroid_spawn_message.y_position = transform_component.y;
            asteroid_spawn_message.rotation = transform_component.rotation;
            asteroid_spawn_message.points = asteroid_portal_component.points;
            asteroid_spawn_message.x_velocity = asteroid_portal_component.x_launch_velocity;
            asteroid_spawn_message.y_velocity = asteroid_portal_component.y_launch_velocity;
            asteroid_spawn_message.angular_velocity = 1;
            asteroid_spawn_message.size = asteroid_portal_component.size;
            asteroid_spawn_message.color = {
                r: color_component.r,
                g: color_component.g,
                b: color_component.b,
                a: color_component.a,
            };
        }
    }
}
