import { Detector, Emits, Entity } from "encompass-ecs";
import { FadeTimer } from "hyperspace/components/timers/fade";
import { ChangeColorMessage } from "hyperspace/messages/change_color";
import { TimerMessage } from "hyperspace/messages/component/timer";
import { RemoveComponentMessage } from "hyperspace/messages/entity/remove_component";

@Emits(RemoveComponentMessage, ChangeColorMessage, TimerMessage)
export class FadeTimerDetector extends Detector {
    public component_types = [ FadeTimer ];

    protected detect(entity: Entity) {
        const fade_timer = entity.get_component(FadeTimer);

        if (fade_timer.time <= 0) {
            const remove_component_message = this.emit_entity_message(RemoveComponentMessage, entity);
            remove_component_message.component_to_remove = fade_timer;
            const fade_message = this.emit_component_message(ChangeColorMessage, fade_timer.color_component);
            fade_message.a = 0;
        } else {
            this.emit_component_message(TimerMessage, fade_timer);
            const fade_message = this.emit_component_message(ChangeColorMessage, fade_timer.color_component);
            fade_message.a = (fade_timer.time / fade_timer.fade_time);
        }
    }
}
