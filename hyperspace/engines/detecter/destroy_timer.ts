import { Detector, Emits, Entity } from "encompass-ecs";
import { DestroyTimer } from "hyperspace/components/timers/destroy";
import { TimerMessage } from "hyperspace/messages/component/timer";
import { DestroyEntityMessage } from "hyperspace/messages/entity/destroy";

@Emits(DestroyEntityMessage, TimerMessage)
export class DestroyTimerDetector extends Detector {
    public component_types = [ DestroyTimer ];

    protected detect(entity: Entity) {
        const destroy_timer = entity.get_component(DestroyTimer);

        if (destroy_timer.time <= 0) {
            this.emit_entity_message(DestroyEntityMessage, entity);
        } else {
            this.emit_component_message(TimerMessage, destroy_timer);
        }
    }
}
