import { Engine, Mutates, Reads, Emits } from "encompass-ecs";
import { LivesComponent } from "hyperspace/components/lives";
import { LifeDecreaseMessage } from "hyperspace/messages/life_decrease";
import { RespawnShipTimerMessage } from "hyperspace/messages/respawn_ship_timer";

@Reads(LifeDecreaseMessage)
@Emits(RespawnShipTimerMessage)
@Mutates(LivesComponent)
export class LivesEngine extends Engine {
    public update() {
        const life_decrease_messages = this.read_messages(LifeDecreaseMessage);
        const lives_components = this.read_components(LivesComponent);

        for (const message of life_decrease_messages.values()) {
            for (const component of lives_components.values()) {
                if (message.player_index === component.player_index) {
                    const mutable_component = this.make_mutable(component);
                    mutable_component.count -= message.amount;

                    if (mutable_component.count > 0) {
                        const respawn_ship_message = this.emit_message(RespawnShipTimerMessage);
                        respawn_ship_message.time = 3;
                    }
                }
            }
        }
    }
}
