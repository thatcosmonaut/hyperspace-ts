/*
-- Public domain:
-- Copyright (C) 2017 by Matthias Richter <vrld@vrld.org>
-- Modified in 2019 by Evan Hemsley <evan@moonside.games>

-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted.
-- THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
-- REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
-- FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
-- INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
-- LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
-- OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
-- PERFORMANCE OF THIS SOFTWARE.
*/

import { Reads, Spawner } from "encompass-ecs";
import { GlowComponent } from "hyperspace/components/draw/effects/glow";
import { GlowSpawnMessage } from "hyperspace/messages/spawn/glow";

@Reads(GlowSpawnMessage)
export class GlowSpawner extends Spawner {
    protected readonly spawn_message_type = GlowSpawnMessage;

    protected spawn(message: GlowSpawnMessage) {
        const glow_entity = this.create_entity();
        const glow_component = glow_entity.add_component(GlowComponent);
        glow_component.layer = message.layer;
        this.initialize_shaders(glow_component, message.strength, message.min_luma);
    }

    private initialize_shaders(glow_component: GlowComponent, strength: number, min_luma: number) {
        const first_blur_shader = this.make_blur_shader(strength);
        first_blur_shader.send("direction", [1 / love.graphics.getWidth(), 0]);

        const second_blur_shader = this.make_blur_shader(strength);
        second_blur_shader.send("direction", [0, 1 / love.graphics.getHeight()]);

        const threshold_shader = love.graphics.newShader(`extern number min_luma;
vec4 effect(vec4 color, Image texture, vec2 tc, vec2 _) {
    vec4 c = Texel(texture, tc);
    number luma = dot(vec3(0.299, 0.587, 0.114), c.rgb);
    return c * step(min_luma, luma) * color;
}`,
        );
        threshold_shader.send("min_luma", min_luma);

        glow_component.first_blur_shader = first_blur_shader;
        glow_component.second_blur_shader = second_blur_shader;
        glow_component.threshold_shader = threshold_shader;
    }

    private make_blur_shader(sigma: number) {
        const support = math.max(1, math.floor(3 * sigma + 0.5));
        const one_by_sigma_sq = sigma > 0 ? 1 / (sigma * sigma) : 1;
        let norm = 0;

        const code = [`extern vec2 direction;
vec4 effect(vec4 color, Image texture, vec2 tc, vec2 _) {
vec4 c = vec4(0.0f);`,
        ];

        let i = -support;
        while (i <= support) {
            const coeff = math.exp(-0.5 * i * i * one_by_sigma_sq);
            norm = norm + coeff;
            code.push(`c += vec4(${coeff}) * Texel(texture, tc + vec2(${i}.0) * direction);`);
            i += 1;
        }

        code.push(`return c * vec4(${1 / norm}) * color;}`);

        return love.graphics.newShader(table.concat(code));
    }
}
