import { Reads, Spawner } from "encompass-ecs";
import { SimpleBloomComponent } from "hyperspace/components/draw/effects/simple_bloom";
import { MusicComponent } from "hyperspace/components/music";
import { MusicSpawnMessage } from "hyperspace/messages/spawn/music";

@Reads(MusicSpawnMessage)
export class MusicSpawner extends Spawner {
    protected readonly spawn_message_type = MusicSpawnMessage;

    protected spawn(message: MusicSpawnMessage) {
        const source = love.audio.newSource(message.file, "stream");

        const entity = this.create_entity();
        const music_component = entity.add_component(MusicComponent);
        music_component.source = source;
        music_component.time = 0;
        music_component.bpm = message.bpm;
        music_component.sections = message.sections;

        const bloom_component = entity.add_component(SimpleBloomComponent);
        bloom_component.layer = 9999;
        bloom_component.power = 0.5;
        bloom_component.shader = love.graphics.newShader("hyperspace/assets/shaders/simple_bloom.frag");
        bloom_component.shader.send("texture_size", [love.graphics.getWidth(), love.graphics.getHeight()]);
        bloom_component.shader.send("power", bloom_component.power);

        source.play();
    }
}
