import { Reads, Spawner } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";
import { CanvasComponent } from "hyperspace/components/draw/canvas";
import { DrivenByPhysics } from "hyperspace/components/driven_by_physics";
import { PhysicsComponent } from "hyperspace/components/physics";
import { AddComponentTimerComponent } from "hyperspace/components/timers/add_component";
import { DestroyTimer } from "hyperspace/components/timers/destroy";
import { FadeTimer } from "hyperspace/components/timers/fade";
import { TransformComponent } from "hyperspace/components/transform";
import { ShipPieceSpawnMessage } from "hyperspace/messages/spawn/ship_piece";
import { IHyperspaceColor } from "hyperspace/types/hyperspace_color";

@Reads(ShipPieceSpawnMessage)
export class ShipPieceSpawner extends Spawner {
    protected readonly spawn_message_type = ShipPieceSpawnMessage;

    private physics_world: World;

    public initialize(physics_world: World) {
        this.physics_world = physics_world;
    }

    protected spawn(message: ShipPieceSpawnMessage) {
        this.build(
            message.x,
            message.y,
            message.rotation,
            message.x_velocity,
            message.y_velocity,
            message.angular_velocity,
            message.length,
            message.color,
        );
    }

    protected build(
        x: number,
        y: number,
        rotation: number,
        x_velocity: number,
        y_velocity: number,
        angular_velocity: number,
        length: number,
        color: IHyperspaceColor,
    ) {
        const entity = this.create_entity();

        const transform_component = entity.add_component(TransformComponent);
        transform_component.x = x;
        transform_component.y = y;
        transform_component.rotation = rotation;
        transform_component.screen_wrap = true;

        const physics_component = entity.add_component(PhysicsComponent);
        physics_component.screen_wrap = true;
        physics_component.body = love.physics.newBody(this.physics_world, x, y, "dynamic");
        physics_component.body.setAngle(rotation);
        physics_component.body.setLinearVelocity(x_velocity, y_velocity);
        physics_component.body.setAngularVelocity(angular_velocity);
        const physics_shape = love.physics.newRectangleShape(length, 2);
        const physics_fixture = love.physics.newFixture(physics_component.body, physics_shape);
        physics_fixture.setUserData(entity);

        entity.add_component(DrivenByPhysics);

        const canvas_component = entity.add_component(CanvasComponent);
        canvas_component.w = length;
        canvas_component.h = 2;
        canvas_component.canvas = love.graphics.newCanvas(length, 2);
        love.graphics.setCanvas(canvas_component.canvas);
        love.graphics.clear();
        love.graphics.setBlendMode("alpha");
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.rectangle("line", 0, 0, length, 2);
        love.graphics.setCanvas();

        const color_component = entity.add_component(ColorComponent);
        color_component.r = color.r;
        color_component.g = color.g;
        color_component.b = color.b;
        color_component.a = color.a;

        const add_fade_timer_component = entity.add_component(AddComponentTimerComponent);
        add_fade_timer_component.time = 8;
        add_fade_timer_component.component_to_add = FadeTimer;
        add_fade_timer_component.args = {
            fade_time: 2,
            time: 2,
            color_component,
        };

        const destroy_timer = entity.add_component(DestroyTimer);
        destroy_timer.time = 10;
    }
}
