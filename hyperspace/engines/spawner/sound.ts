import { Reads, Spawner } from "encompass-ecs";
import { SoundComponent } from "hyperspace/components/sound";
import { MathHelper } from "hyperspace/helpers/math";
import { SoundSpawnMessage } from "hyperspace/messages/spawn/sound";

@Reads(SoundSpawnMessage)
export class SoundSpawner extends Spawner {
    protected readonly spawn_message_type = SoundSpawnMessage;

    protected spawn(message: SoundSpawnMessage) {
        const entity = this.create_entity();
        const source = love.audio.newSource(message.filename, "stream");

        const sound_component = entity.add_component(SoundComponent);
        sound_component.source = source;

        if (message.pitch_variation !== 0) {
            sound_component.source.setPitch(
                1 + MathHelper.randomFloat(
                    -message.pitch_variation,
                    message.pitch_variation,
                ),
            );
        }

        source.play();
    }
}
