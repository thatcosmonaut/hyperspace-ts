import { Emits, Reads, Spawner } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";
import { CanvasComponent } from "hyperspace/components/draw/canvas";
import { FadeInTimerComponent } from "hyperspace/components/fade_in_timer";
import { ShipPortalComponent } from "hyperspace/components/ship_portal";
import { DestroyTimer } from "hyperspace/components/timers/destroy";
import { TransformComponent } from "hyperspace/components/transform";
import { ShipPortalSpawnMessage } from "hyperspace/messages/spawn/ship_portal";
import { SoundSpawnMessage } from "hyperspace/messages/spawn/sound";

@Reads(ShipPortalSpawnMessage)
@Emits(SoundSpawnMessage)
export class ShipPortalSpawner extends Spawner {
    protected readonly spawn_message_type = ShipPortalSpawnMessage;

    private large_ship_canvas: Canvas;

    public initialize() {
        this.large_ship_canvas = love.graphics.newCanvas(256, 256, { msaa: 4 } as love.graphics.CanvasSettings);
        love.graphics.setCanvas(this.large_ship_canvas);
        love.graphics.clear();
        love.graphics.setLineJoin("bevel");
        love.graphics.setBlendMode("alpha");
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.line(128 - 48, 128 - 48, 128, 128 + 96, 128 + 48, 128 - 48, 128 - 48, 128 - 48);
        love.graphics.setCanvas();
    }

    protected spawn(message: ShipPortalSpawnMessage) {
        const entity = this.create_entity();

        const ship_portal_component = entity.add_component(ShipPortalComponent);
        ship_portal_component.time = 3;
        ship_portal_component.x = message.x;
        ship_portal_component.y = message.y;

        for (let i = 0; i < 5; i++) {
            const portal_triangle = this.create_entity();
            const transform_component = portal_triangle.add_component(TransformComponent);
            transform_component.x = message.x;
            transform_component.y = message.y;
            transform_component.rotation = 0;

            const canvas_component = portal_triangle.add_component(CanvasComponent);
            canvas_component.canvas = this.large_ship_canvas;
            canvas_component.w = this.large_ship_canvas.getWidth();
            canvas_component.h = this.large_ship_canvas.getHeight();
            canvas_component.x_scale = 0.5 + i * 0.25;
            canvas_component.y_scale = 0.5 + i * 0.25;

            const fade_in_component = portal_triangle.add_component(FadeInTimerComponent);
            fade_in_component.blank_time = i * (3 / 5);
            fade_in_component.fade_in_time = 3 / 5;
            fade_in_component.time_elapsed = 0;

            const color_component = portal_triangle.add_component(ColorComponent);
            color_component.r = 1;
            color_component.g = 1;
            color_component.b = 1;
            color_component.a = 0;

            const sound_message = this.emit_message(SoundSpawnMessage);
            sound_message.filename = "hyperspace/assets/sounds/respawn.wav";
            sound_message.pitch_variation = 0.02;

            const destroy_timer = portal_triangle.add_component(DestroyTimer);
            destroy_timer.time = 3;
        }
    }
}
