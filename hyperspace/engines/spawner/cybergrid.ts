import { Reads, Spawner } from "encompass-ecs";
import { CybergridComponent } from "hyperspace/components/draw/cybergrid";
import { CybergridSpawnMessage } from "hyperspace/messages/spawn/cybergrid";

import * as PlayMat from "lua-lib/playmat";

@Reads(CybergridSpawnMessage)
export class CybergridSpawner extends Spawner {
    protected readonly spawn_message_type = CybergridSpawnMessage;

    protected spawn(message: CybergridSpawnMessage) {
        const cybergrid = this.create_entity();
        const cybergrid_component = cybergrid.add_component(CybergridComponent);

        cybergrid_component.layer = -90;
        cybergrid_component.width = message.width;
        cybergrid_component.spacing = message.spacing;
        cybergrid_component.x_position = message.x_position;
        cybergrid_component.y_position = message.y_position;
        cybergrid_component.rotation = message.rotation;
        cybergrid_component.zoom = message.zoom;
        cybergrid_component.fov = message.zoom;
        cybergrid_component.offset = message.offset;
        cybergrid_component.desired_speed = message.desired_speed;
        cybergrid_component.speed = message.speed;

        cybergrid_component.grid_canvas = love.graphics.newCanvas(message.width, message.width);
        cybergrid_component.camera = PlayMat.newCamera(
            message.width,
            love.graphics.getHeight(),
            message.x_position,
            message.y_position,
            message.rotation,
            message.zoom,
            message.fov,
            message.offset,
        );

        cybergrid_component.horizontal_lines = [];
        for (let i = -message.width; i <= message.width; i += message.spacing) {
            cybergrid_component.horizontal_lines.push({
                x_start: -message.width * 2,
                y_start: i,
                x_end: message.width * 2,
                y_end: i,
            });
        }

        cybergrid_component.vertical_lines = [];
        for (let i = -message.width * 2; i <= message.width * 2; i += message.spacing) {
            cybergrid_component.vertical_lines.push({
                x_start: i,
                y_start: 0,
                x_end: i,
                y_end: message.width,
            });
        }
    }
}
