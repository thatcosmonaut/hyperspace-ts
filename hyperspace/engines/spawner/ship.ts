import { Entity, Reads, Spawner } from "encompass-ecs";
import { CanShootComponent } from "hyperspace/components/can_shoot";
import { CollisionType, CollisionTypesComponent } from "hyperspace/components/collision_types";
import { CanvasComponent } from "hyperspace/components/draw/canvas";
import { DebugPhysicsDrawComponent } from "hyperspace/components/draw/debug_physics";
import { GunComponent } from "hyperspace/components/gun";
import { PhysicsComponent } from "hyperspace/components/physics";
import { PlayerIndexComponent } from "hyperspace/components/player_index";
import { TransformComponent } from "hyperspace/components/transform";
import { VelocityComponent } from "hyperspace/components/velocity";
import { ShipSpawnMessage } from "hyperspace/messages/spawn/ship";

@Reads(ShipSpawnMessage)
export class ShipSpawner extends Spawner {
    protected readonly spawn_message_type = ShipSpawnMessage;

    private physics_world: World;
    private ship_canvas: Canvas;

    public initialize(physics_world: World, ship_canvas: Canvas) {
        this.physics_world = physics_world;
        this.ship_canvas = ship_canvas;
    }

    protected spawn(message: ShipSpawnMessage) {
        const ship = this.create_entity();
        const player_index_component = ship.add_component(PlayerIndexComponent);
        player_index_component.player_index = 0;

        const transform_component = ship.add_component(TransformComponent);
        transform_component.x = message.x_position;
        transform_component.y = message.y_position;
        transform_component.screen_wrap = true;

        const velocity_component = ship.add_component(VelocityComponent);
        velocity_component.x_linear = message.x_velocity;
        velocity_component.y_linear = message.y_velocity;
        velocity_component.angular = message.angular_velocity;
        velocity_component.max_linear = message.max_linear_speed;
        velocity_component.max_angular = message.max_angular_velocity;

        const canvas_component = ship.add_component(CanvasComponent);
        canvas_component.layer = 0;
        canvas_component.canvas = this.ship_canvas;
        canvas_component.w = 32;
        canvas_component.h = 32;

        const physics_component = ship.add_component(PhysicsComponent);
        physics_component.body = this.create_physics_body(
            ship,
            message.x_position,
            message.y_position,
            message.rotation,
            message.x_velocity,
            message.y_velocity,
            message.angular_velocity,
        );
        physics_component.screen_wrap = true;
        physics_component.max_linear_speed = message.max_linear_speed;
        physics_component.max_angular_velocity = message.max_angular_velocity;

        ship.add_component(GunComponent, {interval: 0.1});
        ship.add_component(CanShootComponent);

        const collision_types_component = ship.add_component(CollisionTypesComponent);
        collision_types_component.collision_types = [ CollisionType.ship ];

        ship.add_component(DebugPhysicsDrawComponent);
    }

    private create_physics_body(
        entity: Entity,
        x_position: number,
        y_position: number,
        rotation: number,
        x_velocity: number,
        y_velocity: number,
        angular_velocity: number,
    ) {
        const collision_body = love.physics.newBody(this.physics_world, x_position, y_position, "dynamic");
        collision_body.setMass(1);
        collision_body.setAngle(rotation);
        collision_body.setLinearVelocity(x_velocity, y_velocity);
        collision_body.setAngularVelocity(angular_velocity);

        const collision_shape = love.physics.newPolygonShape(-6, -6, 0, 12, 6, -6);
        const collision_fixture = love.physics.newFixture(collision_body, collision_shape);
        collision_fixture.setUserData(entity);

        return collision_body;
    }
}
