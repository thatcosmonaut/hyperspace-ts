import { Reads, Spawner } from "encompass-ecs";
import { CollisionType, CollisionTypesComponent } from "hyperspace/components/collision_types";
import { CanvasComponent } from "hyperspace/components/draw/canvas";
import { PhysicsComponent } from "hyperspace/components/physics";
import { DestroyTimer } from "hyperspace/components/timers/destroy";
import { TransformComponent } from "hyperspace/components/transform";
import { VelocityComponent } from "hyperspace/components/velocity";
import { BulletSpawnMessage } from "hyperspace/messages/spawn/bullet";

@Reads(BulletSpawnMessage)
export class BulletSpawner extends Spawner {
    protected readonly spawn_message_type = BulletSpawnMessage;

    private canvas: Canvas;
    private physics_world: World;

    public initialize(physics_world: World) {
        this.physics_world = physics_world;
        this.initialize_canvas();
    }

    protected spawn(message: BulletSpawnMessage) {
        const bullet = this.create_entity();

        const transform_component = bullet.add_component(TransformComponent);
        transform_component.x = message.x_position;
        transform_component.y = message.y_position;
        transform_component.screen_wrap = true;

        const velocity_component = bullet.add_component(VelocityComponent);
        velocity_component.x_linear = message.x_velocity;
        velocity_component.y_linear = message.y_velocity;

        const physics_component = bullet.add_component(PhysicsComponent);
        physics_component.body = love.physics.newBody(
            this.physics_world,
            message.x_position,
            message.y_position,
            "dynamic",
        );
        physics_component.body.setLinearVelocity(message.x_velocity, message.y_velocity);
        physics_component.body.setBullet(true);
        physics_component.body.setMass(1);
        physics_component.screen_wrap = true;

        const collision_shape = love.physics.newRectangleShape(4, 4);
        const collision_fixture = love.physics.newFixture(physics_component.body, collision_shape);
        collision_fixture.setUserData(bullet);
        collision_fixture.setCategory(1);
        collision_fixture.setMask(1);

        const collision_types_component = bullet.add_component(CollisionTypesComponent);
        collision_types_component.collision_types = [ CollisionType.bullet ];

        const canvas_component = bullet.add_component(CanvasComponent);
        canvas_component.layer = 0;
        canvas_component.canvas = this.canvas;
        canvas_component.w = 4;
        canvas_component.h = 4;

        bullet.add_component(DestroyTimer, { time: 2 });
    }

    private initialize_canvas() {
        this.canvas = love.graphics.newCanvas(4, 4);
        love.graphics.setCanvas(this.canvas);
        love.graphics.setBlendMode("alpha");
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.rectangle("fill", 0, 0, 4, 4);
        love.graphics.setCanvas();
    }
}
