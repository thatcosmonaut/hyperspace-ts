import { Reads, Spawner } from "encompass-ecs";
import { AsteroidPortalComponent } from "hyperspace/components/asteroid_portal";
import { ColorComponent } from "hyperspace/components/color";
import { AsteroidFormationComponent } from "hyperspace/components/draw/asteroid_formation";
import { CircleDrawComponent } from "hyperspace/components/draw/circle";
import { TransformComponent } from "hyperspace/components/transform";
import { AsteroidHelper } from "hyperspace/helpers/asteroid";
import { AsteroidPortalSpawnMessage } from "hyperspace/messages/spawn/asteroid_portal";

@Reads(AsteroidPortalSpawnMessage)
export class AsteroidPortalSpawner extends Spawner {
    protected readonly spawn_message_type = AsteroidPortalSpawnMessage;

    protected spawn(message: AsteroidPortalSpawnMessage) {
        const points = AsteroidHelper.generate_points(message.size);

        const entity = this.create_entity();

        const transform_component = entity.add_component(TransformComponent);
        transform_component.x = message.x;
        transform_component.y = message.y;

        const circle_component = entity.add_component(CircleDrawComponent);
        circle_component.radius = 0.01;

        const color_component = entity.add_component(ColorComponent);
        color_component.r = message.color.r;
        color_component.g = message.color.g;
        color_component.b = message.color.b;
        color_component.a = message.color.a;

        const asteroid_portal_component = entity.add_component(AsteroidPortalComponent);
        asteroid_portal_component.grow_time = message.grow_time;
        asteroid_portal_component.hold_time = message.hold_time;
        asteroid_portal_component.size = message.size;
        asteroid_portal_component.points = points;
        asteroid_portal_component.x_launch_velocity = message.x_launch_velocity;
        asteroid_portal_component.y_launch_velocity = message.y_launch_velocity;

        const asteroid_formation_component = entity.add_component(AsteroidFormationComponent);
        asteroid_formation_component.points = points;
        asteroid_formation_component.total_time = message.hold_time;
    }
}
