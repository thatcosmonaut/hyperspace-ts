import { Reads, Spawner } from "encompass-ecs";
import { ScanlinesComponent } from "hyperspace/components/draw/effects/scanlines";
import { ScanlinesSpawnMessage } from "hyperspace/messages/spawn/scanlines";

@Reads(ScanlinesSpawnMessage)
export class ScanlinesSpawner extends Spawner {
    protected readonly spawn_message_type = ScanlinesSpawnMessage;

    protected spawn(message: ScanlinesSpawnMessage) {
        const scanlines_entity = this.create_entity();
        const scanlines_component = scanlines_entity.add_component(ScanlinesComponent);
        scanlines_component.layer = message.layer;
        scanlines_component.width = message.width;
        scanlines_component.phase = message.phase;
        scanlines_component.thickness = message.thickness;
        scanlines_component.opacity = message.opacity;
        scanlines_component.color = message.color;

        scanlines_component.shader.send("width", message.width);
        scanlines_component.shader.send("phase", message.phase);
        scanlines_component.shader.send("thickness", message.thickness);
        scanlines_component.shader.send("opacity", message.opacity);
        scanlines_component.shader.send("color", [message.color.r, message.color.g, message.color.b]);
    }
}
