import { Entity, Reads, Spawner } from "encompass-ecs";
import { AsteroidCounterComponent } from "hyperspace/components/asteroid_counter";
import { AsteroidSizeComponent } from "hyperspace/components/asteroid_size";
import { CollisionType, CollisionTypesComponent } from "hyperspace/components/collision_types";
import { ColorComponent } from "hyperspace/components/color";
import { CanvasComponent } from "hyperspace/components/draw/canvas";
import { DrivenByPhysics } from "hyperspace/components/driven_by_physics";
import { PhysicsComponent } from "hyperspace/components/physics";
import { TransformComponent } from "hyperspace/components/transform";
import { AsteroidSpawnMessage } from "hyperspace/messages/spawn/asteroid";
import { IHyperspaceColor } from "hyperspace/types/hyperspace_color";
import { rotate } from "lua-lib/hump/vectorlight";

@Reads(AsteroidSpawnMessage)
export class AsteroidSpawner extends Spawner {
    protected readonly spawn_message_type = AsteroidSpawnMessage;

    protected physics_world: World;

    public initialize(physics_world: World) {
        this.physics_world = physics_world;
    }

    protected spawn(message: AsteroidSpawnMessage) {
        this.build(
            message.x_position,
            message.y_position,
            message.rotation,
            message.points,
            message.x_velocity,
            message.y_velocity,
            message.angular_velocity,
            message.size,
            message.color,
            message.max_linear_speed,
            message.max_angular_velocity,
        );
    }

    protected build(
        x_position: number,
        y_position: number,
        rotation: number,
        points: number[],
        x_velocity: number,
        y_velocity: number,
        angular_velocity: number,
        size: number,
        color: IHyperspaceColor,
        max_linear_speed = math.huge,
        max_angular_velocity = math.huge,
    ) {
        const asteroid_entity = this.create_entity();

        const asteroid_points = points;
        const polygon_triangles = love.math.triangulate(asteroid_points);

        const asteroid_canvas = love.graphics.newCanvas(
            size,
            size,
            { msaa: 8 } as love.graphics.CanvasSettings,
        );

        const draw_points = [];
        for (const point of asteroid_points) {
            draw_points.push(point + size * 0.5);
        }

        const [r, g, b, a] = love.graphics.getColor();
        love.graphics.setCanvas(asteroid_canvas);
        love.graphics.setBlendMode("alpha");
        love.graphics.setLineWidth(2);
        love.graphics.setColor(0, 0, 0, 1);
        love.graphics.polygon("fill", unpack(draw_points));
        love.graphics.setColor(color.r, color.g, color.b, 1);
        love.graphics.polygon("line", unpack(draw_points));
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.setLineWidth(1);
        love.graphics.setCanvas();

        const color_component = asteroid_entity.add_component(ColorComponent);
        color_component.r = color.r;
        color_component.g = color.g;
        color_component.b = color.b;
        color_component.a = color.a;

        const transform_component = asteroid_entity.add_component(TransformComponent);
        transform_component.x = x_position;
        transform_component.y = y_position;
        transform_component.rotation = rotation;
        [transform_component.forward_x, transform_component.forward_y] = rotate(rotation, 0, 1);
        transform_component.screen_wrap = true;

        const canvas_component = asteroid_entity.add_component(CanvasComponent);
        canvas_component.layer = 0;
        canvas_component.canvas = asteroid_canvas;
        canvas_component.w = size;
        canvas_component.h = size;

        const physics_component = asteroid_entity.add_component(PhysicsComponent);
        physics_component.body = this.create_physics_body(
            asteroid_entity,
            x_position,
            y_position,
            rotation,
            x_velocity,
            y_velocity,
            angular_velocity,
            polygon_triangles,
        );

        physics_component.screen_wrap = true;
        physics_component.max_linear_speed = max_linear_speed;
        physics_component.max_angular_velocity = max_angular_velocity;

        asteroid_entity.add_component(DrivenByPhysics);

        asteroid_entity.add_component(AsteroidCounterComponent);

        const collision_types_component = asteroid_entity.add_component(CollisionTypesComponent);
        collision_types_component.collision_types = [ CollisionType.asteroid ];

        asteroid_entity.add_component(AsteroidSizeComponent, { size });

        // const debug_physics_draw_component = asteroid_entity.add_component(DebugPhysicsDrawComponent);
        // debug_physics_draw_component.layer = 1;
    }

    private create_physics_body(
        entity: Entity,
        x_position: number,
        y_position: number,
        rotation: number,
        x_velocity: number,
        y_velocity: number,
        angular_velocity: number,
        triangles: table,
    ) {
        const physics_body = love.physics.newBody(this.physics_world, x_position, y_position, "dynamic");
        physics_body.setMass(10);
        physics_body.setLinearVelocity(x_velocity, y_velocity);
        physics_body.setAngle(rotation);
        physics_body.setAngularVelocity(angular_velocity);

        /** @luaIterator */
        for (const index in triangles) {
            const triangle = triangles[index];
            const physics_shape = love.physics.newPolygonShape(triangle);
            const physics_fixture = love.physics.newFixture(physics_body, physics_shape);
            physics_fixture.setRestitution(1);
            physics_fixture.setUserData(entity);
            physics_fixture.setCategory(2);
        }

        return physics_body;
    }
}
