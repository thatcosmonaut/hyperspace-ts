import { Reads, Spawner } from "encompass-ecs";
import { ShockwaveComponent } from "hyperspace/components/draw/effects/shockwave";
import { DestroyTimer } from "hyperspace/components/timers/destroy";
import { ShockwaveSpawnMessage } from "hyperspace/messages/spawn/shockwave";

@Reads(ShockwaveSpawnMessage)
export class ShockwaveSpawner extends Spawner {
    protected readonly spawn_message_type = ShockwaveSpawnMessage;

    protected spawn(message: ShockwaveSpawnMessage) {
        this.build(
            message.x,
            message.y,
            message.size,
        );
    }

    protected build(
        x: number,
        y: number,
        size: number,
    ) {
        const entity = this.create_entity();

        const params_x = 10.0;
        let params_y;
        let params_z;

        if (size >= 128) {
            params_y = 0.8;
            params_z = 0.2;
        } else if (size >= 64) {
            params_y = 0.8;
            params_z = 0.1;
        } else if (size >= 16) {
            params_y = 0.8;
            params_z = 0.1;
        } else {
            params_y = 0.8;
            params_z = 0.05;
        }

        const shader = love.graphics.newShader("hyperspace/assets/shaders/shockwave.frag");
        shader.send("center_x", x / love.graphics.getWidth());
        shader.send("center_y", y / love.graphics.getHeight());
        shader.send("time", 0);
        shader.send("params_x", params_x);
        shader.send("params_y", params_y);
        shader.send("params_z", params_z);

        const draw_component = entity.add_component(ShockwaveComponent);
        draw_component.layer = 9000;
        draw_component.shader = shader;
        draw_component.time_elapsed = 0;

        const destroy_timer = entity.add_component(DestroyTimer);
        destroy_timer.time = 1.5;
    }
}
