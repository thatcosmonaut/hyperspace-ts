import { Reads, Spawner } from "encompass-ecs";
import { TimeDilationComponent } from "hyperspace/components/time_dilation";
import { TimeDilationSpawnMessage } from "hyperspace/messages/spawn/time_dilation";

@Reads(TimeDilationSpawnMessage)
export class TimeDilationSpawner extends Spawner {
    protected readonly spawn_message_type = TimeDilationSpawnMessage;

    protected spawn(message: TimeDilationSpawnMessage) {
        const entity = this.create_entity();
        const component = entity.add_component(TimeDilationComponent);

        component.ease_in_time = message.ease_in_time;
        component.duration = message.duration;
        component.ease_out_time = message.ease_out_time;
        component.factor = message.factor;
        component.time_elapsed = 0;
    }
}
