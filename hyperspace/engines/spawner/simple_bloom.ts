import { Reads, Spawner } from "encompass-ecs";
import { SimpleBloomComponent } from "hyperspace/components/draw/effects/simple_bloom";
import { SimpleBloomSpawnMessage } from "hyperspace/messages/spawn/simple_bloom";

@Reads(SimpleBloomSpawnMessage)
export class SimpleBloomSpawner extends Spawner {
    protected readonly spawn_message_type = SimpleBloomSpawnMessage;

    protected spawn(message: SimpleBloomSpawnMessage) {
        const entity = this.create_entity();
        const component = entity.add_component(SimpleBloomComponent);
        component.layer = message.layer;
        component.power = message.power;
        component.shader = love.graphics.newShader("hyperspace/assets/shaders/simple_bloom.frag");
        component.shader.send("texture_size", [love.graphics.getWidth(), love.graphics.getHeight()]);
        component.shader.send("power", component.power);
    }
}
