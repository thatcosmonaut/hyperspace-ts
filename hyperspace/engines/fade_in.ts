import { Detector, Emits, Entity, Mutates, Reads } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";
import { FadeInTimerComponent } from "hyperspace/components/fade_in_timer";
import { ChangeColorMessage } from "hyperspace/messages/change_color";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";

@Reads(TimeDilationBroadcast)
@Emits(ChangeColorMessage)
@Mutates(FadeInTimerComponent)
export class FadeInEngine extends Detector {
    public component_types = [FadeInTimerComponent, ColorComponent];

    public detect(entity: Entity, dt: number) {
        const color_component = entity.get_component(ColorComponent);
        const fade_in_timer_component = this.make_mutable(entity.get_component(FadeInTimerComponent));

        for (const time_dilation_broadcast of this.read_messages(TimeDilationBroadcast).values()) {
            dt *= time_dilation_broadcast.factor;
        }

        fade_in_timer_component.time_elapsed += dt;

        if (fade_in_timer_component.time_elapsed > fade_in_timer_component.blank_time) {
            const fade_percent = fade_in_timer_component.time_elapsed / (fade_in_timer_component.blank_time + fade_in_timer_component.fade_in_time);

            const message = this.emit_component_message(ChangeColorMessage, color_component);
            message.a = fade_percent;
        }
    }
}
