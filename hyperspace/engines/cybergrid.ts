import { Engine, Mutates, Reads } from "encompass-ecs";
import { CybergridComponent } from "hyperspace/components/draw/cybergrid";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";

@Reads(TimeDilationBroadcast)
@Mutates(CybergridComponent)
export class CybergridEngine extends Engine {
    public update(dt: number) {
        const time_dilation_broadcasts = this.read_messages(TimeDilationBroadcast);
        for (const broadcast of time_dilation_broadcasts.values()) {
            dt *= broadcast.factor;
        }

        const cybergrid_components = this.read_components_mutable(CybergridComponent);

        for (const component of cybergrid_components.values()) {
            component.x_position = (component.x_position - component.speed * dt) % (component.width * 0.5);
            component.camera.x = component.x_position;
        }
    }
}
