import { Emits, Engine, Reads } from "encompass-ecs";
import { AsteroidSizeComponent } from "hyperspace/components/asteroid_size";
import { ColorComponent } from "hyperspace/components/color";
import { PhysicsComponent } from "hyperspace/components/physics";
import { TransformComponent } from "hyperspace/components/transform";
import { AsteroidHelper } from "hyperspace/helpers/asteroid";
import { AsteroidSplitSpawnMessage } from "hyperspace/messages/asteroid_split_spawn_message";
import { AsteroidSpawnMessage } from "hyperspace/messages/spawn/asteroid";
import * as vectorlight from "lua-lib/hump/vectorlight";

@Reads(AsteroidSplitSpawnMessage)
@Emits(AsteroidSpawnMessage)
export class AsteroidSplitSpawnEngine extends Engine {
    public update() {
        for (const message of this.read_messages(AsteroidSplitSpawnMessage).values()) {
            const asteroid_entity = message.asteroid_entity;

            const transform_component = asteroid_entity.get_component(TransformComponent);
            const physics_component = asteroid_entity.get_component(PhysicsComponent);
            const asteroid_size_component = asteroid_entity.get_component(AsteroidSizeComponent);
            const color_component = asteroid_entity.get_component(ColorComponent);

            const size = asteroid_size_component.size * 0.5;
            if (size < 25) {
                return;
            }

            const x_position = transform_component.x;
            const y_position = transform_component.y;

            const [x_linear, y_linear] = physics_component.body.getLinearVelocity();

            const [x_normal, y_normal] = vectorlight.normalize(x_linear, y_linear);

            const offset_scalar = size * 0.5 + 10;

            const [first_x_velocity, first_y_velocity] = vectorlight.rotate(
                -math.pi / 4,
                x_linear,
                y_linear,
            );
            const [first_x_direction, first_y_direction] = vectorlight.mul(
                offset_scalar,
                ...vectorlight.rotate(-math.pi / 4, x_normal, y_normal),
            );
            const [first_x_position, first_y_position] = vectorlight.add(
                x_position,
                y_position,
                first_x_direction,
                first_y_direction,
            );

            const first_spawn_message = this.emit_message(AsteroidSpawnMessage);
            first_spawn_message.x_position = first_x_position;
            first_spawn_message.y_position = first_y_position;
            first_spawn_message.rotation = 0;
            first_spawn_message.points = AsteroidHelper.generate_points(size);
            first_spawn_message.x_velocity = first_x_velocity;
            first_spawn_message.y_velocity = first_y_velocity;
            first_spawn_message.angular_velocity = 0;
            first_spawn_message.size = size;
            first_spawn_message.color = {
                r: color_component.r,
                g: color_component.g,
                b: color_component.b,
                a: color_component.a,
            };

            const [second_x_velocity, second_y_velocity] = vectorlight.rotate(
                math.pi / 4,
                x_linear,
                y_linear,
            );
            const [second_x_direction, second_y_direction] = vectorlight.mul(
                offset_scalar,
                ...vectorlight.rotate(math.pi / 4, x_normal, y_normal),
            );
            const [second_x_position, second_y_position] = vectorlight.add(
                x_position,
                y_position,
                second_x_direction,
                second_y_direction,
            );

            const second_spawn_message = this.emit_message(AsteroidSpawnMessage);
            second_spawn_message.x_position = second_x_position;
            second_spawn_message.y_position = second_y_position;
            second_spawn_message.rotation = 0;
            second_spawn_message.points = AsteroidHelper.generate_points(size);
            second_spawn_message.x_velocity = second_x_velocity;
            second_spawn_message.y_velocity = second_y_velocity;
            second_spawn_message.angular_velocity = 0;
            second_spawn_message.size = size;
            second_spawn_message.color = {
                r: color_component.r,
                g: color_component.g,
                b: color_component.b,
                a: color_component.a,
            };
        }
    }
}
