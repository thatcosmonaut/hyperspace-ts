import { Engine, Mutates, Reads } from "encompass-ecs";
import { ParticleSystemContainerComponent } from "hyperspace/components/draw/effects/particle_system_container";
import { EmitExplodeParticleBroadcast } from "hyperspace/messages/emit_explode_particle";
import { TimeDilationBroadcast } from "hyperspace/messages/time_dilation";

@Reads(TimeDilationBroadcast, EmitExplodeParticleBroadcast)
@Mutates(ParticleSystemContainerComponent)
export class ParticleSystemProcessor extends Engine {
    public update(dt: number) {
        for (const particle_system_container_component of this.read_components(ParticleSystemContainerComponent).values()) {
            const particle_system_container = particle_system_container_component.particle_system_container;

            for (const broadcast of this.read_messages(TimeDilationBroadcast).values()) {
                dt *= broadcast.factor;
            }

            for (const broadcast of this.read_messages(EmitExplodeParticleBroadcast).values()) {
                const particle_system = particle_system_container.get_or_create_particle_system(
                    broadcast.r,
                    broadcast.g,
                    broadcast.b,
                );

                particle_system.moveTo(broadcast.x, broadcast.y);
                particle_system.emit(broadcast.amount);
            }

            for (const particle_system of particle_system_container.particle_systems.entries()) {
                particle_system.update(dt);
            }
        }
    }
}
