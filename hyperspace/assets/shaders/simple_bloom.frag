#define glare_base_factor 0.9
#define pi 3.14159

extern vec2 texture_size;
extern float power;

vec4 effect(vec4 vcolor, Image texture, vec2 texcoord, vec2 pixel_coords)
{
	vec4 texcolor = Texel(texture, texcoord);
	vec2 glare_factor = vec2(glare_base_factor) / texture_size;

	vec4 s = vec4(0.0);
	vec4 b = vec4(0.0);

	for (int i = -2; i < 2; i++)
	{
		for (int j = -1; j < 1; j++)
		{
			s += Texel(texture, texcoord + vec2(-i, j)*glare_factor) * power;
			b += Texel(texture, texcoord + vec2(j, i)*glare_factor) * power;
		}
	}

	vec4 finalcolor = vec4(0.0, 0.0, 0.0, 1.0);

	if (texcolor.r < 2.0)
	{
		finalcolor = s*s*s*0.001+b*b*b*0.0080 + texcolor;
	}

	finalcolor.a = 1.0;

	return finalcolor;
}