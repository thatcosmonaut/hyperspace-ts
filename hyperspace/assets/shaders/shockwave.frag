extern float center_x;
extern float center_y;
extern float time;
extern float params_x;
extern float params_y;
extern float params_z;

vec4 effect(vec4 color, Image texture, vec2 texcoord, vec2 pixel_coords)
{
  vec2 center = vec2(center_x, center_y);
  vec3 waveParams = vec3(params_x, params_y, params_z);

  float myTime = time * 0.5;

  vec2 resolution = vec2(1280, 720);

  vec2 uv = pixel_coords / resolution;
  // Distance to the center
  float dist = distance(uv, center);

  // Original color
	vec4 c = texture2D(texture, uv);

  // Limit to waves
	if (dist >= waveParams.z * 0.1 && dist <= myTime + waveParams.z && dist >= myTime - waveParams.z) {
      // The pixel offset distance based on the input parameters
		float diff = (dist - myTime);
    float diffDist = min(distance(dist, myTime + waveParams.z), distance(dist, myTime - waveParams.z));
		float diffPow = (1.0 - pow(abs(diff * waveParams.x), waveParams.y * diffDist));
		float diffTime = (diff  * diffPow);

    // The direction of the distortion
		vec2 dir = normalize(uv - center);

    // Perform the distortion and reduce the effect over time
		uv += ((dir * diffTime) / (myTime * dist * 80.0));

    // Grab color for the new coord
		c = texture2D(texture, uv);

    // Optionally: Blow out the color for brighter-energy origin
    c += (c * diffPow) / (myTime * dist * 40.0);
	}

  return c;
}
