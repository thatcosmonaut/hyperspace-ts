export interface ISection {
    time: number;
    intensity: number;
}

export interface ISections {
    default: number;
    sections: ISection[];
}
