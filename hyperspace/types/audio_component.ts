import { MusicComponent } from "hyperspace/components/music";
import { SoundComponent } from "hyperspace/components/sound";

export type AudioComponent = MusicComponent | SoundComponent;
