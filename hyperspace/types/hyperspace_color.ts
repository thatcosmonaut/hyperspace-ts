export interface IHyperspaceColor {
    r: number;
    g: number;
    b: number;
    a: number;
}
