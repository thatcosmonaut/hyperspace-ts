import { GameState } from "./gamestate";
import { Waves } from "./gamestates/waves";

export class Hyperspace {
    private current_game_state: GameState;
    private next_game_state: GameState;
    private switching = false;

    private waves: Waves;

    private canvas: Canvas;

    public switch_game_state(game_state: GameState) {
        this.switching = true;
        this.next_game_state = game_state;
    }

    public load() {
        this.waves = new Waves();
        this.canvas = love.graphics.newCanvas();
        this.switch_game_state(this.waves);
    }

    public update(dt: number) {
        if (this.switching) {
            this.perform_game_state_switch();
        }
        this.current_game_state.update(dt);
    }

    public draw() {
        love.graphics.clear();
        love.graphics.setCanvas(this.canvas);
        love.graphics.clear();
        this.current_game_state.draw();
        love.graphics.setCanvas();
        love.graphics.setBlendMode("alpha", "premultiplied");
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.draw(this.canvas);
    }

    private perform_game_state_switch() {
        if (this.current_game_state !== null) {
            this.current_game_state.exit();
        }

        this.next_game_state.enter();
        this.current_game_state = this.next_game_state;
        this.switching = false;
    }
}
