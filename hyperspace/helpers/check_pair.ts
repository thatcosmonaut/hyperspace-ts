export class CheckPairHelper {
    public static check_pairs(a: any, b: any, c: any, d: any): boolean {
        return (a === c && b === d) || (a === d) && (b === c);
    }
}
