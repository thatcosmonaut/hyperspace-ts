export class ParticleSystemHelper {
    public static create_particle_system(r: number, g: number, b: number): ParticleSystem {
        const explode_particle_system = love.graphics.newParticleSystem(this.create_particle(), 4000);

        explode_particle_system.setColors(
            r, g, b, 1,
            r, g, b, 1,
        );

        explode_particle_system.setSpread(math.pi * 2);
        explode_particle_system.setSpeed(100, 200);
        explode_particle_system.setSizes(2, 1, 0);
        explode_particle_system.setSizeVariation(1);
        explode_particle_system.setParticleLifetime(3, 5);
        explode_particle_system.setEmissionArea("ellipse", 20, 20);

        return explode_particle_system;
    }

    public static create_particle(): Canvas {
        const particle_canvas = love.graphics.newCanvas(1, 1);
        love.graphics.setCanvas(particle_canvas);
        love.graphics.setBlendMode("alpha");
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.rectangle("fill", 0, 0, 1, 1);
        love.graphics.setCanvas();

        return particle_canvas;
    }
}
