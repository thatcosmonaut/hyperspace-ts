export class MathHelper {
    public static clamp(value: number, min: number, max: number): number {
        return math.max(min, math.min(value, max));
    }

    public static randomFloat(low: number, high: number): number {
        return love.math.random() + love.math.random(low, high);
    }
}
