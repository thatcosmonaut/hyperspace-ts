import { MathHelper } from "hyperspace/helpers/math";
import * as vectorlight from "lua-lib/hump/vectorlight";

export class AsteroidHelper {
    public static generate_points(size: number): number[] {
        const points = [];

        const start_x = 0;
        const start_y = 0;
        let angle = 0;
        const point_count = love.math.random(5, 8);

        let small = false;

        let i = 1;
        while (i < point_count) {
            let distance;
            if (small || love.math.random() < 0.8) {
                distance = MathHelper.randomFloat(size * 0.25, size * 0.5);
            } else {
                distance = MathHelper.randomFloat(1, size * 0.25);
                small = true;
            }

            const [rotated_x, rotated_y] = vectorlight.rotate(angle, 0, 1);
            const [relative_x, relative_y] = vectorlight.mul(distance, rotated_x, rotated_y);

            const [point_x, point_y] = vectorlight.add(
                start_x,
                start_y,
                relative_x,
                relative_y,
            );

            points.push(math.floor(point_x));
            points.push(math.floor(point_y));
            angle += (math.pi * 2) / point_count;
            i += 1;
        }

        return points;
    }
}
