import { GCOptimizedSet } from "tstl-gc-optimized-collections";
import { ParticleSystemHelper } from "./particle_system";

export class ParticleSystemContainer {
    private rgb_to_particle_system = new Map<number, Map<number, Map<number, ParticleSystem>>>();
    private _particle_systems = new GCOptimizedSet<ParticleSystem>();

    get particle_systems() {
        return this._particle_systems;
    }

    public get_or_create_particle_system(r: number, g: number, b: number): ParticleSystem {
        let particle_system: ParticleSystem;

        if (this.rgb_to_particle_system.has(r)) {
            const red = this.rgb_to_particle_system.get(r)!;
            if (red.has(g)) {
                const green = red.get(g)!;
                if (green.has(b)) {
                    particle_system = green.get(b)!;
                } else {
                    green.set(b, particle_system = ParticleSystemHelper.create_particle_system(r, g, b));
                    this._particle_systems.add(particle_system);
                }
            } else {
                const green = new Map<number, ParticleSystem>();
                green.set(b, particle_system = ParticleSystemHelper.create_particle_system(r, g, b));
                red.set(g, green);
                this._particle_systems.add(particle_system);
            }
        } else {
            const blue = new Map<number, Map<number, ParticleSystem>>();
            const green = new Map<number, ParticleSystem>();
            green.set(b, particle_system = ParticleSystemHelper.create_particle_system(r, g, b));
            blue.set(g, green);
            this.rgb_to_particle_system.set(r, blue);
            this._particle_systems.add(particle_system);
        }

        return particle_system;
    }
}
