export class CollisionData {
    public active: boolean;
    public x1: number;
    public y1: number;
    public x2?: number;
    public y2?: number;
    public friction: number;
    public x_normal: number;
    public y_normal: number;
    public restitution: number;
}
