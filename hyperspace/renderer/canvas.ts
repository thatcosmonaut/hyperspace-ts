import { EntityRenderer } from "encompass-ecs";
import { Entity } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";
import { CanvasComponent } from "hyperspace/components/draw/canvas";
import { TransformComponent } from "hyperspace/components/transform";

export class CanvasRenderer extends EntityRenderer {
    public readonly component_types = [ TransformComponent ];
    public readonly draw_component_type = CanvasComponent;

    public render(entity: Entity) {
        const transform_component = entity.get_component(TransformComponent);
        const canvas_draw_component = entity.get_component(CanvasComponent);

        const x = transform_component.x;
        const y = transform_component.y;
        const r = transform_component.rotation;

        const component_canvas = canvas_draw_component.canvas;
        const w = canvas_draw_component.w;
        const h = canvas_draw_component.h;

        const x_scale = canvas_draw_component.x_scale || 1;
        const y_scale = canvas_draw_component.y_scale || 1;

        const [mode, alpha_mode] = love.graphics.getBlendMode();
        love.graphics.setBlendMode("alpha", "alphamultiply");

        if (entity.has_component(ColorComponent)) {
            const color_component = entity.get_component(ColorComponent);
            love.graphics.setColor(color_component.r, color_component.g, color_component.b, color_component.a);
        } else {
            love.graphics.setColor(1, 1, 1, 1);
        }

        love.graphics.draw(component_canvas, x, y, r, x_scale, y_scale, w * 0.5, h * 0.5);
        love.graphics.setBlendMode(mode, alpha_mode);
        love.graphics.setColor(1, 1, 1, 1);
    }
}
