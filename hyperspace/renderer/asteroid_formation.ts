import { Entity, EntityRenderer } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";
import { AsteroidFormationComponent } from "hyperspace/components/draw/asteroid_formation";
import { TransformComponent } from "hyperspace/components/transform";
import * as Easing from "lua-lib/easing";

export class AsteroidFormationRenderer extends EntityRenderer {
    public draw_component_type = AsteroidFormationComponent;
    public component_types = [TransformComponent, ColorComponent];

    public render(entity: Entity) {
        const transform_component = entity.get_component(TransformComponent);
        const asteroid_formation_component = entity.get_component(AsteroidFormationComponent);
        const points = asteroid_formation_component.points;

        if (asteroid_formation_component.time_elapsed > 0) {
            let i = 0;
            while (i < points.length - 2) {
                const first_x = points[i];
                const first_y = points[i + 1];

                const second_x = points[i + 2];
                const second_y = points[i + 3];

                const [interp_second_x, interp_second_y] = this.interp_point(
                    first_x,
                    first_y,
                    second_x,
                    second_y,
                    asteroid_formation_component.time_elapsed,
                    asteroid_formation_component.total_time,
                );

                this.draw_formation_line(transform_component, first_x, first_y, interp_second_x, interp_second_y);

                i += 2;
            }

            const x1 = points[points.length - 2];
            const y1 = points[points.length - 1];

            const x2 = points[0];
            const y2 = points[1];

            const [interp_x2, interp_y2] = this.interp_point(
                x1,
                y1,
                x2,
                y2,
                asteroid_formation_component.time_elapsed,
                asteroid_formation_component.total_time,
            );

            this.draw_formation_line(transform_component, x1, y1, interp_x2, interp_y2);
        }
    }

    /** @returnTuple */
    private interp_point(x1: number, y1: number, x2: number, y2: number, time_elapsed: number, total_time: number): [number, number] {
        const interp_second_x = Easing.linear(
            time_elapsed,
            x1,
            x2 - x1,
            total_time,
        );

        const interp_second_y = Easing.linear(
            time_elapsed,
            y1,
            y2 - y1,
            total_time,
        );

        return [interp_second_x, interp_second_y];
    }

    private draw_formation_line(transform_component: TransformComponent, x1: number, y1: number, x2: number, y2: number): void {
        love.graphics.setColor(1, 1, 1, 0.4);
        x1 += transform_component.x;
        y1 += transform_component.y;
        x2 += transform_component.x;
        y2 += transform_component.y;
        love.graphics.line(x1, y1, x2, y2);
        love.graphics.circle("fill", x2, y2, 2);
    }
}
