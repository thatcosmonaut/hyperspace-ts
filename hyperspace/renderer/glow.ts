import { EntityRenderer } from "encompass-ecs";
import { Entity } from "encompass-ecs";
import { GlowComponent } from "hyperspace/components/draw/effects/glow";

export class GlowRenderer extends EntityRenderer {
    public component_types = [];
    public draw_component_type = GlowComponent;

    protected front = love.graphics.newCanvas();
    protected back = love.graphics.newCanvas();

    public render(entity: Entity) {
        const canvas = love.graphics.getCanvas();
        const glow_component = entity.get_component(GlowComponent);

        love.graphics.setBlendMode("alpha", "premultiplied");
        love.graphics.setColor(1, 1, 1, 1);

        love.graphics.setCanvas(this.front);
        love.graphics.clear();
        love.graphics.setShader(glow_component.threshold_shader);
        love.graphics.draw(canvas);

        love.graphics.setCanvas(this.back);
        love.graphics.clear();
        love.graphics.setShader(glow_component.first_blur_shader);
        love.graphics.draw(this.front);

        love.graphics.setCanvas(this.front);
        love.graphics.clear();

        love.graphics.setShader();
        love.graphics.setBlendMode("add", "premultiplied");
        love.graphics.draw(canvas);

        love.graphics.setShader(glow_component.second_blur_shader);
        love.graphics.draw(this.back);

        love.graphics.setCanvas(canvas);
        love.graphics.setBlendMode("add", "premultiplied");
        love.graphics.draw(this.front);

        love.graphics.setShader();
    }
}
