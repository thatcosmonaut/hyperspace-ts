import { EntityRenderer } from "encompass-ecs";
import { Entity } from "encompass-ecs";
import { SimpleBloomComponent } from "hyperspace/components/draw/effects/simple_bloom";

export class SimpleBloomRenderer extends EntityRenderer {
    public component_types = [];
    public draw_component_type = SimpleBloomComponent;

    protected canvas = love.graphics.newCanvas();

    public render(entity: Entity) {
        const canvas = love.graphics.getCanvas();
        const simple_bloom_component = entity.get_component(SimpleBloomComponent);

        love.graphics.setBlendMode("alpha", "premultiplied");
        love.graphics.setColor(1, 1, 1, 1);

        love.graphics.setCanvas(this.canvas);
        love.graphics.clear();
        love.graphics.setShader(simple_bloom_component.shader);
        love.graphics.draw(canvas);

        love.graphics.setCanvas(canvas);
        love.graphics.setShader();
        love.graphics.clear();
        love.graphics.draw(this.canvas);
    }
}
