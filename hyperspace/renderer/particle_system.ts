import { Entity, EntityRenderer } from "encompass-ecs";
import { ParticleSystemContainerComponent } from "hyperspace/components/draw/effects/particle_system_container";

export class ParticleSystemRenderer extends EntityRenderer {
    public component_types = [];
    public draw_component_type = ParticleSystemContainerComponent;

    public render(entity: Entity) {
        const particle_system_draw_component = entity.get_component(ParticleSystemContainerComponent);
        const particle_systems = particle_system_draw_component.particle_system_container.particle_systems;

        for (const particle_system of particle_systems.entries()) {
            const [mode, alphamode] = love.graphics.getBlendMode();
            love.graphics.setBlendMode("add");
            love.graphics.setColor(1, 1, 1, 1);
            love.graphics.draw(particle_system);
            love.graphics.setBlendMode(mode, alphamode);
        }
    }
}
