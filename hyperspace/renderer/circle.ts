import { EntityRenderer } from "encompass-ecs";
import { Entity } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";
import { CircleDrawComponent } from "hyperspace/components/draw/circle";
import { TransformComponent } from "hyperspace/components/transform";

export class CircleRenderer extends EntityRenderer {
    public readonly component_types = [ TransformComponent ];
    public readonly draw_component_type = CircleDrawComponent;

    public render(entity: Entity) {
        const transform_component = entity.get_component(TransformComponent);
        const circle_draw_component = entity.get_component(CircleDrawComponent);

        love.graphics.setBlendMode("alpha");
        if (entity.has_component(ColorComponent)) {
            const color_component = entity.get_component(ColorComponent);
            love.graphics.setColor(color_component.r, color_component.g, color_component.b, color_component.a);
        } else {
            love.graphics.setColor(1, 1, 1, 1);
        }
        love.graphics.circle("line", transform_component.x, transform_component.y, circle_draw_component.radius);
    }
}
