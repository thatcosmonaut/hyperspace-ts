import { GeneralRenderer } from "encompass-ecs";
import { ComboCounterComponent } from "hyperspace/components/combo_counter";

export class ComboRenderer extends GeneralRenderer {
    public layer = 10000;

    public render() {
        const combo_components = this.read_components(ComboCounterComponent);

        for (const combo_component of combo_components.values()) {
            love.graphics.setBlendMode("alpha");
            love.graphics.setColor(1, 1, 1, 1);

            love.graphics.printf(
                "x" + combo_component.count,
                (love.graphics.getWidth() * 0.8) - 10,
                love.graphics.getHeight() * 0.05,
                love.graphics.getWidth() * 0.2,
                "right",
            );

            love.graphics.rectangle(
                "fill",
                love.graphics.getWidth() * 0.8 + (love.graphics.getWidth() * 0.2 * (1.0 - combo_component.percentage_time_remaining)) - 10,
                love.graphics.getHeight() * 0.1,
                love.graphics.getWidth() * 0.2 * combo_component.percentage_time_remaining,
                love.graphics.getHeight() * 0.01,
            );
        }
    }
}
