import { EntityRenderer } from "encompass-ecs";
import { Entity } from "encompass-ecs";
import { CybergridComponent } from "hyperspace/components/draw/cybergrid";

import * as PlayMat from "lua-lib/playmat";

interface ILine {
    x_start: number;
    y_start: number;
    x_end: number;
    y_end: number;
}

export class CybergridRenderer extends EntityRenderer {
    public readonly component_types = [ ];
    public readonly draw_component_type = CybergridComponent;

    public render(entity: Entity) {
        const cybergrid_component = entity.get_component(CybergridComponent);

        love.graphics.setColor(70 / 255, 173 / 255, 212 / 255, 1);
        love.graphics.setBlendMode("alpha");

        for (const line of cybergrid_component.horizontal_lines) {
            this.draw_cybergrid_line(cybergrid_component, line);
        }

        for (const line of cybergrid_component.vertical_lines) {
            this.draw_cybergrid_line(cybergrid_component, line);
        }
    }

    private draw_cybergrid_line(cybergrid_component: Readonly<CybergridComponent>, line: ILine) {
        const [start_x, start_y] = PlayMat.toScreen(cybergrid_component.camera, line.x_start, line.y_start);
        const [end_x, end_y] = PlayMat.toScreen(cybergrid_component.camera, line.x_end, line.y_end);

        love.graphics.line(start_x, start_y, end_x, end_y);
    }
}
