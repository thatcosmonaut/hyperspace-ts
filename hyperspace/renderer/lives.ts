import { GeneralRenderer } from "encompass-ecs";
import { LivesComponent } from "hyperspace/components/lives";

export class LivesRenderer extends GeneralRenderer {
    public layer = 10000;

    private ship_canvas: Canvas;

    public initialize(ship_canvas: Canvas) {
        this.ship_canvas = ship_canvas;
    }

    public render() {
        const lives_components = this.read_components(LivesComponent);

        for (const lives_component of lives_components.values()) {
            love.graphics.setBlendMode("alpha");
            love.graphics.setColor(1, 1, 1, 1);

            let draw_position = 10;
            for (let i = 0; i < lives_component.count; i++) {
                love.graphics.draw(this.ship_canvas, draw_position, 16, math.pi, 1, 1, 16, 16);
                draw_position += 15;
            }
        }
    }
}
