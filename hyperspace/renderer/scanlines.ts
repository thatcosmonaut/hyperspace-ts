import { Entity, EntityRenderer } from "encompass-ecs";
import { ScanlinesComponent } from "hyperspace/components/draw/effects/scanlines";

export class ScanlinesRenderer extends EntityRenderer {
    public component_types = [];
    public draw_component_type = ScanlinesComponent;

    protected scanlines_canvas = love.graphics.newCanvas();

    public render(entity: Entity) {
        const scanlines_draw_component = entity.get_component(ScanlinesComponent);

        const [mode, alphamode] = love.graphics.getBlendMode();
        const canvas = love.graphics.getCanvas();

        love.graphics.setBlendMode("alpha", "premultiplied");
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.setCanvas(this.scanlines_canvas);
        love.graphics.clear();
        love.graphics.setShader(scanlines_draw_component.shader);
        love.graphics.draw(canvas);
        love.graphics.setShader();

        love.graphics.setCanvas(canvas);
        love.graphics.clear();
        love.graphics.draw(this.scanlines_canvas);

        love.graphics.setBlendMode(mode, alphamode);
    }
}
