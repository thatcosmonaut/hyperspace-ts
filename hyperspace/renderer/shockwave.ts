import { Entity, EntityRenderer } from "encompass-ecs";
import { ShockwaveComponent } from "hyperspace/components/draw/effects/shockwave";

export class ShockwaveRenderer extends EntityRenderer {
    public component_types = [];
    public draw_component_type = ShockwaveComponent;

    private canvas = love.graphics.newCanvas();

    public render(entity: Entity) {
        const shockwave_component = entity.get_component(ShockwaveComponent);

        const canvas = love.graphics.getCanvas();

        love.graphics.setCanvas(this.canvas);
        love.graphics.setBlendMode("alpha", "premultiplied");
        love.graphics.clear();
        love.graphics.setShader(shockwave_component.shader);
        love.graphics.draw(canvas);

        love.graphics.setCanvas(canvas);
        love.graphics.setShader();
        love.graphics.clear();
        love.graphics.draw(this.canvas);
    }
}
