import { Entity, EntityRenderer } from "encompass-ecs";
import { DebugPhysicsDrawComponent } from "hyperspace/components/draw/debug_physics";
import { PhysicsComponent } from "hyperspace/components/physics";

export class DebugPhysicsRenderer extends EntityRenderer {
    public component_types = [ PhysicsComponent ];
    public draw_component_type = DebugPhysicsDrawComponent;

    public render(entity: Entity) {
        const physics_component = entity.get_component(PhysicsComponent);

        const physics_body = physics_component.body;
        const fixture_list = physics_body.getFixtures();

        love.graphics.setBlendMode("alpha");
        love.graphics.setColor(0, 0, 0, 1);
        /** @luaIterator */
        for (const key in fixture_list) {
            const fixture = fixture_list[key] as Fixture;
            const shape = fixture.getShape() as PolygonShape;
            const points = shape.getPoints();
            love.graphics.polygon("fill", ...physics_body.getWorldPoints(...points));
        }

        love.graphics.setColor(1, 1, 1, 1);
        /** @luaIterator */
        for (const key in fixture_list) {
            const fixture = fixture_list[key] as Fixture;
            const shape = fixture.getShape() as PolygonShape;
            const points = shape.getPoints();
            love.graphics.polygon("line", ...physics_body.getWorldPoints(...points));
        }
    }
}
