import { Message } from "encompass-ecs";

export class TimeDilationBroadcast extends Message {
    public factor: number;
}
