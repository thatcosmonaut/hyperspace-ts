import { Message } from "encompass-ecs";

export class BulletSpawnMessage extends Message {
    public x_position: number;
    public y_position: number;
    public x_velocity: number;
    public y_velocity: number;
}
