import { Message } from "encompass-ecs";
import { IHyperspaceColor } from "hyperspace/types/hyperspace_color";

export class ShipPieceSpawnMessage extends Message {
    public x: number;
    public y: number;
    public rotation: number;
    public x_velocity: number;
    public y_velocity: number;
    public angular_velocity: number;
    public length: number;
    public color: IHyperspaceColor;
}
