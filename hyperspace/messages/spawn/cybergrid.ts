import { Message } from "encompass-ecs";

export class CybergridSpawnMessage extends Message {
    public layer: number;
    public width: number;
    public spacing: number;
    public x_position: number;
    public y_position: number;
    public rotation: number;
    public zoom: number;
    public fov: number;
    public offset: number;
    public desired_speed: number;
    public speed: number;
}
