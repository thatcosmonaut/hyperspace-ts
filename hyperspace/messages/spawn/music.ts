import { Message } from "encompass-ecs";
import { ISections } from "hyperspace/types/music_section";

export class MusicSpawnMessage extends Message {
    public file: string;
    public sections: ISections;
    public bpm: number;
}
