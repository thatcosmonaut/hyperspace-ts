import { Message } from "encompass-ecs";
import { IHyperspaceColor } from "hyperspace/types/hyperspace_color";

export class AsteroidPortalSpawnMessage extends Message {
    public x: number;
    public y: number;
    public size: number;
    public x_launch_velocity: number;
    public y_launch_velocity: number;
    public grow_time: number;
    public hold_time: number;
    public color: IHyperspaceColor;
}
