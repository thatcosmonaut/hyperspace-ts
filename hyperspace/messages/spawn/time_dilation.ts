import { Message } from "encompass-ecs";

export class TimeDilationSpawnMessage extends Message {
    public ease_in_time: number;
    public duration: number;
    public ease_out_time: number;
    public factor: number;
}
