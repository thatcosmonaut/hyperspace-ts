import { Message } from "encompass-ecs";

export class ShipSpawnMessage extends Message {
    public player_index: number;
    public x_position: number;
    public y_position: number;
    public rotation: number;
    public x_velocity: number;
    public y_velocity: number;
    public angular_velocity: number;
    public max_linear_speed: number;
    public max_angular_velocity: number;
}
