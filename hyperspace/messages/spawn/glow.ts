import { Message } from "encompass-ecs";

export class GlowSpawnMessage extends Message {
    public layer: number;
    public strength: number;
    public min_luma: number;
}
