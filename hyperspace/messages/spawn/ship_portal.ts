import { Message } from "encompass-ecs";

export class ShipPortalSpawnMessage extends Message {
    public x: number;
    public y: number;
    public time: number;
}
