import { Message } from "encompass-ecs";

export class AsteroidSpawnMessage extends Message {
    public x_position: number;
    public y_position: number;
    public rotation: number;
    public points: number[];
    public x_velocity: number;
    public y_velocity: number;
    public angular_velocity: number;
    public size: number;
    public color: {r: number, g: number, b: number, a: number};
    public max_linear_speed = math.huge;
    public max_angular_velocity = math.huge;
}
