import { Message } from "encompass-ecs";

export class SoundSpawnMessage extends Message {
    public filename: string;
    public pitch_variation: number;
}
