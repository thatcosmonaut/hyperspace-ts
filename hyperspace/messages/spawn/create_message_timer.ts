import { Message, MessageUserDataOnly, Type } from "encompass-ecs";

export class CreateMessageTimerSpawnMessage<TMessage extends Message> extends Message {
    public time: number;
    public message_to_create: Type<TMessage>;
    public message_args: MessageUserDataOnly<TMessage>;
}
