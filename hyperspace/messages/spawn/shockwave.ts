import { Message } from "encompass-ecs";

export class ShockwaveSpawnMessage extends Message {
    public x: number;
    public y: number;
    public size: number;
}
