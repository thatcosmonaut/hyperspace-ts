import { Message } from "encompass-ecs";

export class ScanlinesSpawnMessage extends Message {
    public layer: number;
    public width: number;
    public phase: number;
    public thickness: number;
    public opacity: number;
    public color: {r: number, g: number, b: number};
}
