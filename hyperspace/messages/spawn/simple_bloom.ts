import { Message } from "encompass-ecs";

export class SimpleBloomSpawnMessage extends Message {
    public layer: number;
    public power: number;
}
