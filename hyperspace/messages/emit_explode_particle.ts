import { Message } from "encompass-ecs";

export class EmitExplodeParticleBroadcast extends Message {
    public x: number;
    public y: number;
    public r: number;
    public g: number;
    public b: number;
    public a: number;
    public amount: number;
}
