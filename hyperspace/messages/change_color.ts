import { ComponentMessage, Message } from "encompass-ecs";
import { ColorComponent } from "hyperspace/components/color";

export class ChangeColorMessage extends Message implements ComponentMessage {
    public component: ColorComponent;
    public r: number;
    public g: number;
    public b: number;
    public a: number;
}
