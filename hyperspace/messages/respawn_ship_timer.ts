import { Message } from "encompass-ecs";

export class RespawnShipTimerMessage extends Message {
    public player_index: number;
    public time: number;
}
