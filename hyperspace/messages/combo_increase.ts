import { Message } from "encompass-ecs";

export class ComboIncreaseMessage extends Message {
    public amount: number;
}
