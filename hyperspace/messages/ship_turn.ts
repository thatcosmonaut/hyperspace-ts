import { Message } from "encompass-ecs";

export class ShipTurnMessage extends Message {
    public amount: number;
}
