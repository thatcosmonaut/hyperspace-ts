import { ComponentMessage, Message } from "encompass-ecs";
import { MusicComponent } from "hyperspace/components/music";

export class AudioMessage extends Message implements ComponentMessage {
    public component: MusicComponent;
}
