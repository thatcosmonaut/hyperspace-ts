import { ComponentMessage, Message } from "encompass-ecs";
import { CircleDrawComponent } from "hyperspace/components/draw/circle";

export class CircleMessage extends Message implements ComponentMessage {
    public component: CircleDrawComponent;
    public radius_delta: number;
}
