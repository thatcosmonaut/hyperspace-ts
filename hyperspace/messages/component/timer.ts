import { ComponentMessage, Message } from "encompass-ecs";
import { TimerComponent } from "hyperspace/components/timer";

export class TimerMessage extends Message implements ComponentMessage {
    public component: TimerComponent;
}
