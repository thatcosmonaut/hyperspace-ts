import { ComponentMessage, Message } from "encompass-ecs";
import { SimpleBloomComponent } from "hyperspace/components/draw/effects/simple_bloom";

export class SimpleBloomMessage extends Message implements ComponentMessage {
    public component: SimpleBloomComponent;
    public power: number;
}
