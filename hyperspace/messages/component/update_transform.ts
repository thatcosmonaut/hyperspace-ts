import { ComponentMessage, Message } from "encompass-ecs";
import { TransformComponent } from "hyperspace/components/transform";

export class UpdateTransformMessage extends Message implements ComponentMessage {
    public component: Readonly<TransformComponent>;
    public x_delta: number;
    public y_delta: number;
    public rotation_delta: number;
    public instant_linear: boolean;
    public instant_angular: boolean;
}
