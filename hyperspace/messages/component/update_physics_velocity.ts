import { ComponentMessage, Message } from "encompass-ecs";
import { PhysicsComponent } from "hyperspace/components/physics";

export class UpdatePhysicsVelocityMessage extends Message implements ComponentMessage {
    public component: Readonly<PhysicsComponent>;
    public x_thrust: number;
    public y_thrust: number;
    public torque: number;
    public instant_thrust: boolean;
    public instant_torque: boolean;
}
