import { ComponentMessage, Message } from "encompass-ecs";
import { VelocityComponent } from "hyperspace/components/velocity";

export class UpdateVelocityMessage extends Message implements ComponentMessage {
    public component: VelocityComponent;
    public x_delta: number;
    public y_delta: number;
    public angular_delta: number;
    public instant_thrust: boolean;
    public instant_torque: boolean;
}
