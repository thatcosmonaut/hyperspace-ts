import { ComponentMessage, Message } from "encompass-ecs";
import { PhysicsComponent } from "hyperspace/components/physics";

export class UpdatePhysicsComponentMessage extends Message implements ComponentMessage {
    public component: PhysicsComponent;
}
