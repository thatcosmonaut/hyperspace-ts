import { ComponentMessage, Message } from "encompass-ecs";
import { ParticleSystemContainerComponent } from "hyperspace/components/draw/effects/particle_system_container";

export class ParticleSystemContainerMessage extends Message implements ComponentMessage {
    public component: ParticleSystemContainerComponent;
}
