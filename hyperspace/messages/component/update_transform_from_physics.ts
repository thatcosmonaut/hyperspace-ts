import { ComponentMessage, Message } from "encompass-ecs";
import { PhysicsComponent } from "hyperspace/components/physics";

export class UpdateTransformFromPhysicsMessage extends Message implements ComponentMessage {
    public component: PhysicsComponent;
}
