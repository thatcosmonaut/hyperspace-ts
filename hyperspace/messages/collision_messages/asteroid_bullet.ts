import { Entity, Message } from "encompass-ecs";
import { CollisionData } from "hyperspace/collision_data";

export class AsteroidBulletCollisionMessage extends Message {
    public asteroid_entity: Entity;
    public bullet_entity: Entity;
    public collision_data: CollisionData;
}
