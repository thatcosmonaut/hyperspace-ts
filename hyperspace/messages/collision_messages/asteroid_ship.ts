import { Entity, Message } from "encompass-ecs";
import { CollisionData } from "hyperspace/collision_data";

export class AsteroidShipCollisionMessage extends Message {
    public asteroid_entity: Entity;
    public ship_entity: Entity;
    public collision_data: CollisionData;
}
