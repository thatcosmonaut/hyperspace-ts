import { Entity, Message } from "encompass-ecs";
import { CollisionData } from "hyperspace/collision_data";

export class AsteroidAsteroidCollisionMessage extends Message {
    public asteroid_entity_one: Entity;
    public asteroid_entity_two: Entity;
    public collision_data: CollisionData;
}
