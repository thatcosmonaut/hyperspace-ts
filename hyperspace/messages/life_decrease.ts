import { Message } from "encompass-ecs";

export class LifeDecreaseMessage extends Message {
    public player_index: number;
    public amount: number;
}
