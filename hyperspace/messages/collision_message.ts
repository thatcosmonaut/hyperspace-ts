import { Entity, Message } from "encompass-ecs";
import { CollisionData } from "hyperspace/collision_data";
import { CollisionType } from "hyperspace/components/collision_types";

export class CollisionMessage extends Message {
    public entity_one: Entity;
    public entity_two: Entity;
    public collision_type_one: CollisionType;
    public collision_type_two: CollisionType;
    public collision_data: CollisionData;
}
