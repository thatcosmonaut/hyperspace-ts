import { Entity, EntityMessage, Message } from "encompass-ecs";

export class DestroyEntityMessage extends Message implements EntityMessage {
    public entity: Entity;
}
