import { Component, Entity, EntityMessage, Message } from "encompass-ecs";

export class RemoveComponentMessage extends Message implements EntityMessage {
    public entity: Entity;
    public component_to_remove: Component;
}
