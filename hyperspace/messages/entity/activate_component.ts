import { Component, Entity, EntityMessage, Message } from "encompass-ecs";

export class ActivateComponentMessage extends Message implements EntityMessage {
    public entity: Entity;
    public component: Component;
}
