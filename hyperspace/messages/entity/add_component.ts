import { Component, ComponentUserDataOnly, Entity, EntityMessage, Message, Type } from "encompass-ecs";

export class AddComponentMessage<TComponent extends Component> extends Message implements EntityMessage {
    public entity: Entity;
    public component_to_add: Type<TComponent>;
    public args: ComponentUserDataOnly<TComponent>;
}
