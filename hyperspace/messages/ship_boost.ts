import { Message } from "encompass-ecs";

export class ShipBoostMessage extends Message {
    public factor: number;
}
