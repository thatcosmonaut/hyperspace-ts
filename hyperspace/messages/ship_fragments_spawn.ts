import { Entity, Message } from "encompass-ecs";

export class ShipFragmentsSpawnMessage extends Message {
    public ship_entity: Entity;
}
