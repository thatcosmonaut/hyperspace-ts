import { Entity, Message } from "encompass-ecs";

export class AsteroidSplitSpawnMessage extends Message {
    public asteroid_entity: Entity;
}
