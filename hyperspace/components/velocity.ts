import { Component } from "encompass-ecs";

export class VelocityComponent extends Component {
    public x_linear = 0;
    public y_linear = 0;
    public angular = 0;
    public max_linear = math.huge;
    public max_angular = math.huge;
}
