import { Component } from "encompass-ecs";

export class RespawnShipTimerComponent extends Component {
    public player_index: number;
    public time: number;
}
