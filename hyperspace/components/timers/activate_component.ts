import { Component } from "encompass-ecs";

export class ActivateComponentTimerComponent extends Component {
    public time: number;
    public component_to_activate: Component;
}
