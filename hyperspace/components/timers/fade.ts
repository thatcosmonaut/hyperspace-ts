import { ColorComponent } from "../color";
import { TimerComponent } from "../timer";

export class FadeTimer extends TimerComponent {
    public color_component: ColorComponent;
    public fade_time: number;
}
