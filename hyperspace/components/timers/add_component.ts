import { Component, ComponentUserDataOnly, Type } from "encompass-ecs";

export class AddComponentTimerComponent<TComponent extends Component> extends Component {
    public time: number;
    public component_to_add: Type<TComponent>;
    public args: ComponentUserDataOnly<TComponent>;
}
