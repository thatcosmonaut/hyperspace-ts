import { Component } from "encompass-ecs";
import { ISections } from "hyperspace/types/music_section";

export class MusicComponent extends Component {
    public source: Source;
    public time: number;
    public bpm: number;
    public sections: ISections;

    protected on_destroy() {
        this.source.stop();
    }
}
