import { Component } from "encompass-ecs";

export class AsteroidPortalComponent extends Component {
    public grow_time: number;
    public hold_time: number;
    public time_elapsed = 0;
    public size: number;
    public points: number[];
    public x_launch_velocity: number;
    public y_launch_velocity: number;
}
