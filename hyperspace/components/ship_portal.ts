import { DrawComponent } from "encompass-ecs";

export class ShipPortalComponent extends DrawComponent {
    public time: number;
    public x: number;
    public y: number;
}
