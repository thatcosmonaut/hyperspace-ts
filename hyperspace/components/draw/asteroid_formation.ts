import { DrawComponent } from "encompass-ecs";

export class AsteroidFormationComponent extends DrawComponent {
    public points: number[];
    public time_elapsed = 0;
    public total_time: number;
}
