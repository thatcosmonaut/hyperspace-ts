import { DrawComponent } from "encompass-ecs";

export class CircleDrawComponent extends DrawComponent {
    public radius = 1;
}
