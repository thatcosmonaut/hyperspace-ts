import { DrawComponent } from "encompass-ecs";
import * as PlayMat from "lua-lib/playmat";

interface ILine {
    x_start: number;
    y_start: number;
    x_end: number;
    y_end: number;
}

export class CybergridComponent extends DrawComponent {
    public width: number;
    public spacing: number;
    public x_position: number;
    public y_position: number;
    public rotation: number;
    public zoom: number;
    public fov: number;
    public offset: number;
    public desired_speed: number;
    public speed: number;

    public camera: PlayMat.Camera;
    public grid_canvas: Canvas;
    public horizontal_lines: ILine[];
    public vertical_lines: ILine[];
}
