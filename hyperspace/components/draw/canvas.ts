import { DrawComponent } from "encompass-ecs";

export class CanvasComponent extends DrawComponent {
    public canvas: Canvas;
    public w: number;
    public h: number;
    public x_scale: number;
    public y_scale: number;
}
