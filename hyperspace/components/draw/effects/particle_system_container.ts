import { DrawComponent } from "encompass-ecs";
import { ParticleSystemContainer } from "hyperspace/helpers/particle_system_container";

export class ParticleSystemContainerComponent extends DrawComponent {
    public particle_system_container: ParticleSystemContainer;
}
