import { DrawComponent } from "encompass-ecs";

export class GlowComponent extends DrawComponent {
    public strength: number;
    public min_luma: number;

    public first_blur_shader: Shader;
    public second_blur_shader: Shader;
    public threshold_shader: Shader;
}
