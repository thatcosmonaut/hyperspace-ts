import { DrawComponent } from "encompass-ecs";

export class ShockwaveComponent extends DrawComponent {
    public shader: Shader;
    public time_elapsed: number;
}
