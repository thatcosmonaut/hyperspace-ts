import { DrawComponent } from "encompass-ecs";

export class SimpleBloomComponent extends DrawComponent {
    public power: number;
    public shader: Shader;
}
