import { DrawComponent } from "encompass-ecs";

export class ScanlinesComponent extends DrawComponent {
    public width: number;
    public phase: number;
    public thickness: number;
    public opacity: number;
    public color: {r: number, g: number, b: number};

    public shader = love.graphics.newShader("hyperspace/assets/shaders/scanlines.frag");
}
