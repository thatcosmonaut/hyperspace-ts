import { Component } from "encompass-ecs";

export class FadeInTimerComponent extends Component {
    public blank_time: number;
    public fade_in_time: number;
    public time_elapsed: number;
}
