import { Component } from "encompass-ecs";

export class TransformComponent extends Component {
    public x: number = 0;
    public y: number = 0;
    public rotation: number = 0;
    public forward_x: number = 0;
    public forward_y: number = 1;
    public screen_wrap: boolean = false;
}
