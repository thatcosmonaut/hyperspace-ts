import { Component } from "encompass-ecs";

export enum WaveState {
    preStart,
    spawning,
    waitingForSpawn,
    inProgress,
    ending,
}

export class WaveControlComponent extends Component {
    public wave: number;
    public asteroid_count = 0;
    public wave_state = WaveState.preStart;
    public asteroid_spawned = false;
    public time = 1;
}
