import { Component } from "encompass-ecs";
import { CollisionResponseComponent } from "../collision_response";

export class TemporarilyDeactivateComponentsCollisionResponse extends CollisionResponseComponent {
    public time: number;
    public components_to_deactivate: Component[];
}
