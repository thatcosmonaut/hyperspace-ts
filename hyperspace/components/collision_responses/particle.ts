import { CollisionResponseComponent } from "../collision_response";

export class ParticleCollisionResponse extends CollisionResponseComponent {
    public amount: number;
    public r: number;
    public g: number;
    public b: number;
    public a: number;
}
