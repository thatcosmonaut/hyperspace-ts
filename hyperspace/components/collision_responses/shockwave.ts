import { CollisionResponseComponent } from "../collision_response";

export class ShockwaveCollisionResponse extends CollisionResponseComponent {
    public size: number;
}
