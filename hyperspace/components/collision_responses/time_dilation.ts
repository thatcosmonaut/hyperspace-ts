import { CollisionResponseComponent } from "../collision_response";

export class TimeDilationCollisionResponse extends CollisionResponseComponent {
    public ease_in_time: number;
    public duration: number;
    public ease_out_time: number;
    public factor: number;
}
