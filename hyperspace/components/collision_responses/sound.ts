import { CollisionResponseComponent } from "../collision_response";

export class SoundCollisionResponse extends CollisionResponseComponent {
    public file: string;
    public pitch_variation = 0;
}
