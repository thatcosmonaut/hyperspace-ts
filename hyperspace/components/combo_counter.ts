import { Component } from "encompass-ecs";

export class ComboCounterComponent extends Component {
    public combo_window = 5;
    public count = 0;
    public percentage_time_remaining = 0;
    public time_remaining = 0;
}
