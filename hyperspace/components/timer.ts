import { Component } from "encompass-ecs";

export abstract class TimerComponent extends Component {
    public time: number;
}
