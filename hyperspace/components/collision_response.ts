import { Component, Type } from "encompass-ecs";
import { CollisionType } from "./collision_types";

export class CollisionResponseComponent extends Component {
    public collision_types: Type<CollisionType>[];
}
