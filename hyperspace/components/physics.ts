import { Component } from "encompass-ecs";

export class PhysicsComponent extends Component {
    public body: Body;
    public screen_wrap = false;
    public max_linear_speed = math.huge;
    public max_angular_velocity = math.huge;

    protected on_destroy() {
        if (!this.body.isDestroyed()) {
            this.body.destroy();
        }
    }

    protected on_activate() {
        this.body.setActive(true);
    }

    protected on_deactivate() {
        this.body.setActive(false);
    }
}
