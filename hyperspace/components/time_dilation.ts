import { Component } from "encompass-ecs";

export class TimeDilationComponent extends Component {
    public ease_in_time: number;
    public duration: number;
    public ease_out_time: number;
    public factor: number;
    public time_elapsed: number;
}
