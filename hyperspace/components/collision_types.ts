import { Component } from "encompass-ecs";

export enum CollisionType {
    asteroid,
    bullet,
    ship,
}

export class CollisionTypesComponent extends Component {
    public collision_types: CollisionType[];
}
