import { Component } from "encompass-ecs";

export class ColorComponent extends Component {
    public r: number;
    public g: number;
    public b: number;
    public a: number;
}
