import { World as EncompassWorld, WorldBuilder } from "encompass-ecs";
import { ComboCounterComponent } from "hyperspace/components/combo_counter";
import { ParticleSystemContainerComponent } from "hyperspace/components/draw/effects/particle_system_container";
import { LivesComponent } from "hyperspace/components/lives";
import { WaveControlComponent } from "hyperspace/components/wave_control";
import { AsteroidSplitSpawnEngine } from "hyperspace/engines/asteroid_split_spawn";
import { AsteroidAsteroidCollisionResolver } from "hyperspace/engines/collision/asteroid_asteroid";
import { AsteroidBulletCollisionResolver } from "hyperspace/engines/collision/asteroid_bullet";
import { AsteroidShipCollisionResolver } from "hyperspace/engines/collision/asteroid_ship";
import { CollisionResolver } from "hyperspace/engines/collision/collision_response";
import { ComboEngine } from "hyperspace/engines/combo";
import { CybergridEngine } from "hyperspace/engines/cybergrid";
import { ActivateComponentTimerDetector } from "hyperspace/engines/detecter/activate_component_timer";
import { AddComponentTimerDetector } from "hyperspace/engines/detecter/add_component_timer";
import { AsteroidPortalDetector } from "hyperspace/engines/detecter/asteroid_portal";
import { BeatBloomDetector } from "hyperspace/engines/detecter/beat_bloom";
import { DestroyTimerDetector } from "hyperspace/engines/detecter/destroy_timer";
import { FadeTimerDetector } from "hyperspace/engines/detecter/fade_timer";
import { MusicDetector } from "hyperspace/engines/detecter/music";
import { ParticleSystemDetector } from "hyperspace/engines/detecter/particle_system";
import { ShipMovementDetector } from "hyperspace/engines/detecter/ship_movement";
import { ShipShootDetector } from "hyperspace/engines/detecter/ship_shoot";
import { SoundDetector } from "hyperspace/engines/detecter/sound";
import { TimeDilationDetector } from "hyperspace/engines/detecter/time_dilation";
import { WaveControlDetector } from "hyperspace/engines/detecter/wave_control";
import { FadeInEngine } from "hyperspace/engines/fade_in";
import { InputHandlerEngine } from "hyperspace/engines/input_handler";
import { LivesEngine } from "hyperspace/engines/lives";
import { AudioModifier } from "hyperspace/engines/modifier/component/audio";
import { CircleModifier } from "hyperspace/engines/modifier/component/circle";
import { ColorEngine } from "hyperspace/engines/modifier/component/color";
import { SimpleBloomModifier } from "hyperspace/engines/modifier/component/simple_bloom";
import { TimerModifier } from "hyperspace/engines/modifier/component/timer";
import { ActivateComponentModifier } from "hyperspace/engines/modifier/entity/activate_component";
import { AddComponentModifier } from "hyperspace/engines/modifier/entity/add_component";
import { DeactivateComponentModifier } from "hyperspace/engines/modifier/entity/deactivate_component";
import { DestroyEntityModifier } from "hyperspace/engines/modifier/entity/destroy";
import { RemoveComponentModifier } from "hyperspace/engines/modifier/entity/remove_component";
import { PhysicsEngine } from "hyperspace/engines/motion/physics";
import { PrePhysicsAccelerationEngine } from "hyperspace/engines/motion/pre_physics_acceleration";
import { PrePhysicsMotionEngine } from "hyperspace/engines/motion/pre_physics_motion";
import { TransformUpdateEngine } from "hyperspace/engines/motion/transform_update";
import { VelocityUpdateEngine } from "hyperspace/engines/motion/velocity_update";
import { ParticleSystemProcessor } from "hyperspace/engines/particle_system";
import { RespawnShipTimerEngine } from "hyperspace/engines/respawn_ship_timer";
import { ShipFragmentsSpawner } from "hyperspace/engines/ship_fragments_spawner";
import { ShipPortalEngine } from "hyperspace/engines/ship_portal";
import { ShockwaveEngine } from "hyperspace/engines/shockwave";
import { AsteroidSpawner } from "hyperspace/engines/spawner/asteroid";
import { AsteroidPortalSpawner } from "hyperspace/engines/spawner/asteroid_portal";
import { BulletSpawner } from "hyperspace/engines/spawner/bullet";
import { CybergridSpawner } from "hyperspace/engines/spawner/cybergrid";
import { GlowSpawner } from "hyperspace/engines/spawner/glow";
import { MusicSpawner } from "hyperspace/engines/spawner/music";
import { ScanlinesSpawner } from "hyperspace/engines/spawner/scanlines";
import { ShipSpawner } from "hyperspace/engines/spawner/ship";
import { ShipPieceSpawner } from "hyperspace/engines/spawner/ship_piece";
import { ShipPortalSpawner } from "hyperspace/engines/spawner/ship_portal";
import { ShockwaveSpawner } from "hyperspace/engines/spawner/shockwave";
import { SimpleBloomSpawner } from "hyperspace/engines/spawner/simple_bloom";
import { SoundSpawner } from "hyperspace/engines/spawner/sound";
import { TimeDilationSpawner } from "hyperspace/engines/spawner/time_dilation";
import { ParticleSystemContainer } from "hyperspace/helpers/particle_system_container";
import { ChangeColorMessage } from "hyperspace/messages/change_color";
import { CybergridSpawnMessage } from "hyperspace/messages/spawn/cybergrid";
import { GlowSpawnMessage } from "hyperspace/messages/spawn/glow";
import { MusicSpawnMessage } from "hyperspace/messages/spawn/music";
import { ScanlinesSpawnMessage } from "hyperspace/messages/spawn/scanlines";
import { ShipSpawnMessage } from "hyperspace/messages/spawn/ship";
import { ShipPortalSpawnMessage } from "hyperspace/messages/spawn/ship_portal";
import { AsteroidFormationRenderer } from "hyperspace/renderer/asteroid_formation";
import { CanvasRenderer } from "hyperspace/renderer/canvas";
import { CircleRenderer } from "hyperspace/renderer/circle";
import { ComboRenderer } from "hyperspace/renderer/combo";
import { CybergridRenderer } from "hyperspace/renderer/cybergrid";
import { DebugPhysicsRenderer } from "hyperspace/renderer/debug_physics";
import { GlowRenderer } from "hyperspace/renderer/glow";
import { LivesRenderer } from "hyperspace/renderer/lives";
import { ParticleSystemRenderer } from "hyperspace/renderer/particle_system";
import { ScanlinesRenderer } from "hyperspace/renderer/scanlines";
import { ShockwaveRenderer } from "hyperspace/renderer/shockwave";
import { SimpleBloomRenderer } from "hyperspace/renderer/simple_bloom";
import { GameState } from "../gamestate";

export class Waves extends GameState {
    private world: EncompassWorld;
    private physics_world: World;
    private ship_canvas: Canvas;

    public enter() {
        this.initialize_ship_canvas();

        const world_builder = new WorldBuilder();
        this.physics_world = love.physics.newWorld();

        this.add_detecters(world_builder);
        this.add_modifiers(world_builder);
        this.add_spawners(world_builder);
        this.add_processors(world_builder);
        this.add_resolvers(world_builder);
        this.add_renderers(world_builder);

        const particle_system_container_entity = world_builder.create_entity();
        const particle_system_container_component = particle_system_container_entity.add_component(
            ParticleSystemContainerComponent,
        );
        particle_system_container_component.particle_system_container = new ParticleSystemContainer();

        const lives_entity = world_builder.create_entity();
        const lives_component = lives_entity.add_component(LivesComponent);
        lives_component.count = 3;
        lives_component.player_index = 0;

        const combo_counter_entity = world_builder.create_entity();
        combo_counter_entity.add_component(ComboCounterComponent);

        const ship_portal_message = world_builder.emit_message(ShipPortalSpawnMessage);
        ship_portal_message.x = 640;
        ship_portal_message.y = 360;
        ship_portal_message.time = 3;

        // const ship_spawn_message = world_builder.emit_message(ShipSpawnMessage);
        // ship_spawn_message.player_index = 0;
        // ship_spawn_message.x_position = 640;
        // ship_spawn_message.y_position = 360;
        // ship_spawn_message.rotation = 0;
        // ship_spawn_message.x_velocity = 0;
        // ship_spawn_message.y_velocity = 0;
        // ship_spawn_message.angular_velocity = 0;
        // ship_spawn_message.max_linear_speed = 1000;
        // ship_spawn_message.max_angular_velocity = math.huge;

        const cybergrid_spawn_message = world_builder.emit_message(CybergridSpawnMessage);
        cybergrid_spawn_message.layer = -90;
        cybergrid_spawn_message.width = love.graphics.getWidth();
        cybergrid_spawn_message.spacing = 128;
        cybergrid_spawn_message.x_position = 0;
        cybergrid_spawn_message.y_position = love.graphics.getHeight();
        cybergrid_spawn_message.rotation = math.pi * 0.5;
        cybergrid_spawn_message.zoom = love.graphics.getWidth();
        cybergrid_spawn_message.fov = 1;
        cybergrid_spawn_message.offset = 0.5;
        cybergrid_spawn_message.desired_speed = 0;
        cybergrid_spawn_message.speed = 200;

        const glow_spawn_message = world_builder.emit_message(GlowSpawnMessage);
        glow_spawn_message.layer = 9998;
        glow_spawn_message.strength = 5;
        glow_spawn_message.min_luma = 0.5;

        const scanlines_message = world_builder.emit_message(ScanlinesSpawnMessage);
        scanlines_message.layer = 10000;
        scanlines_message.width = 1;
        scanlines_message.phase = 0;
        scanlines_message.thickness = 1;
        scanlines_message.opacity = 0.25;
        scanlines_message.color = {r: 0, g: 0, b: 0};

        const music_message = world_builder.emit_message(MusicSpawnMessage);
        music_message.file = "hyperspace/assets/music/give_you_love.mp3";
        music_message.bpm = 123;
        music_message.sections = {
            default: 0.6,
            sections: [
                { time: 15, intensity: 0.2 },
                { time: 46, intensity: 0.3 },
                { time: 78, intensity: 0.4 },
                { time: 93, intensity: 0.2 },
                { time: 101, intensity: 0.3 },
                { time: 141, intensity: 0.6 },
                { time: 156, intensity: 0.3 },
            ],
        };

        const wave_control_entity = world_builder.create_entity();
        const wave_control_component = wave_control_entity.add_component(WaveControlComponent);
        wave_control_component.wave = 0;

        this.world = world_builder.build();

        print(this.world.engine_order.size);
        for (const [_, engine] of this.world.engine_order.entries()) {
            print(engine.constructor.name);
        }
    }

    public exit() {
        throw("Method not implemented.");
    }

    public update(dt: number) {
        this.world.update(dt);
    }

    public draw() {
        this.world.draw();
    }

    private add_detecters(world_builder: WorldBuilder) {
        world_builder.add_engine(AddComponentTimerDetector);
        world_builder.add_engine(CybergridEngine);
        world_builder.add_engine(DestroyTimerDetector);
        world_builder.add_engine(ShipMovementDetector);
        world_builder.add_engine(ShipShootDetector);
        world_builder.add_engine(ShockwaveEngine);
        world_builder.add_engine(ParticleSystemDetector);
        world_builder.add_engine(TimeDilationDetector);
        world_builder.add_engine(MusicDetector);
        world_builder.add_engine(BeatBloomDetector);
        world_builder.add_engine(AsteroidPortalDetector);
        world_builder.add_engine(FadeTimerDetector);
        world_builder.add_engine(SoundDetector);
        world_builder.add_engine(WaveControlDetector);
        world_builder.add_engine(ActivateComponentTimerDetector);
    }

    private add_modifiers(world_builder: WorldBuilder) {
        world_builder.add_engine(TimerModifier);
        world_builder.add_engine(AudioModifier);
        world_builder.add_engine(SimpleBloomModifier);
        world_builder.add_engine(CircleModifier);
        world_builder.add_engine(PrePhysicsAccelerationEngine);
        world_builder.add_engine(PrePhysicsMotionEngine);
        world_builder.add_engine(VelocityUpdateEngine);
        world_builder.add_engine(TransformUpdateEngine);
        const motion_processor = world_builder.add_engine(PhysicsEngine);
        motion_processor.initialize(this.physics_world);

        world_builder.add_engine(ActivateComponentModifier);
        world_builder.add_engine(AddComponentModifier);
        world_builder.add_engine(DeactivateComponentModifier);
        world_builder.add_engine(DestroyEntityModifier);
        world_builder.add_engine(RemoveComponentModifier);
    }

    private add_spawners(world_builder: WorldBuilder) {
        world_builder.add_engine(AsteroidSpawner).initialize(this.physics_world);
        world_builder.add_engine(AsteroidPortalSpawner);
        world_builder.add_engine(BulletSpawner).initialize(this.physics_world);
        world_builder.add_engine(CybergridSpawner);
        world_builder.add_engine(GlowSpawner);
        world_builder.add_engine(ShipSpawner).initialize(this.physics_world, this.ship_canvas);
        world_builder.add_engine(ShockwaveSpawner);
        world_builder.add_engine(SimpleBloomSpawner);
        world_builder.add_engine(ScanlinesSpawner);
        world_builder.add_engine(TimeDilationSpawner);
        world_builder.add_engine(MusicSpawner);
        world_builder.add_engine(ShipPieceSpawner).initialize(this.physics_world);
        world_builder.add_engine(SoundSpawner);
        world_builder.add_engine(AsteroidSplitSpawnEngine);
        world_builder.add_engine(ShipFragmentsSpawner);
    }

    private add_processors(world_builder: WorldBuilder) {
        world_builder.add_engine(InputHandlerEngine).initialize();
        world_builder.add_engine(ParticleSystemProcessor);
        world_builder.add_engine(ComboEngine);
        world_builder.add_engine(LivesEngine);
        world_builder.add_engine(ColorEngine);
        world_builder.add_engine(ShipPortalSpawner).initialize();
        world_builder.add_engine(ShipPortalEngine);
        world_builder.add_engine(FadeInEngine);
        world_builder.add_engine(RespawnShipTimerEngine);
    }

    private add_resolvers(world_builder: WorldBuilder) {
        world_builder.add_engine(CollisionResolver);
        world_builder.add_engine(AsteroidAsteroidCollisionResolver);
        world_builder.add_engine(AsteroidBulletCollisionResolver);
        world_builder.add_engine(AsteroidShipCollisionResolver);
    }

    private add_renderers(world_builder: WorldBuilder) {
        world_builder.add_renderer(CanvasRenderer);
        world_builder.add_renderer(CircleRenderer);
        world_builder.add_renderer(CybergridRenderer);
        world_builder.add_renderer(GlowRenderer);
        world_builder.add_renderer(SimpleBloomRenderer);
        world_builder.add_renderer(ShockwaveRenderer);
        world_builder.add_renderer(DebugPhysicsRenderer);
        world_builder.add_renderer(ParticleSystemRenderer);
        world_builder.add_renderer(ScanlinesRenderer);
        world_builder.add_renderer(AsteroidFormationRenderer);
        world_builder.add_renderer(ComboRenderer);
        world_builder.add_renderer(LivesRenderer).initialize(this.ship_canvas);
    }

    private initialize_ship_canvas() {
        this.ship_canvas = love.graphics.newCanvas(32, 32, {format: "normal", msaa: 4} as love.graphics.CanvasSettings);
        love.graphics.setCanvas(this.ship_canvas);
        love.graphics.clear();
        love.graphics.setLineJoin("bevel");
        love.graphics.setBlendMode("alpha");
        love.graphics.setColor(0, 0, 0, 1);
        love.graphics.polygon("fill", 16 - 6, 16 - 6, 16 + 0, 16 + 12, 16 + 6, 16 - 6, 16 - 6, 16 - 6);
        love.graphics.setColor(1, 1, 1, 1);
        love.graphics.line(16 - 6, 16 - 6, 16 + 0, 16 + 12, 16 + 6, 16 - 6, 16 - 6, 16 - 6);
        love.graphics.setCanvas();
    }
}
